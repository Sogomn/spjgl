# Simple pure Java game library
Creative name, eh?

[![](https://img.shields.io/badge/documentation-online-blue.svg?longCache=true&style=for-the-badge)](https://sogomn.gitlab.io/spjgl/javadoc)

---

## Motivation
I initially made this to save some coding time. I used to start a new project and dump the old one every week.
Hence I had to write all the basic stuff over and over again. That made me create a library for games.

It is made with only the standard Java API and thus highly portable.

---

## Example
```java
final Clock gameClock = new Clock();
final Screen screen = new Screen(800, 600, "My game");

gameClock.addListener(delta -> {
	System.out.println("Game is updating!");
	System.out.println("Delta time: " + delta);
});

screen.addListener(graphics -> {
	graphics.setColor(Color.RED);
	graphics.drawString("Awesome text on the screen", 50, 50);
});
screen.show();

while (screen.isOpen()) {
	gameClock.update();
	screen.redraw();
}
```

---

## Things this thing can do
* Simple input / update / drawing chain
* Sprite sheet parsing
* Modular UI components
* Camera movement
* Animations
* Sounds
* Network classes
* 2D noise (e.g. for terrain generation)
* Read and write files and images easily
* And more stuff
* Fully documented and maintained

---

## Building
```
git clone https://gitlab.com/Sogomn/spjgl.git
cd spjgl
./gradlew clean jar javadoc
```

If you're on Windows use the `gradlew.bat` instead.	
Afterwards you can find the JAR and the documentation files in the `build` directory.

---

## Documentation
#### [Website](https://sogomn.gitlab.io/spjgl/book "SPJGL website")
#### [Documentation](https://sogomn.gitlab.io/spjgl/javadoc "Documentation")

---

## Note!
If running an application in full screen mode I recommend you to restrict the drawing rate or start a second thread and / or cap the drawing rate manually.	
Full screen caps the frame rate at 60 on most devices and thus makes the update chain lag.	
Also you need to handle synchronization with the event queue yourself.

---

## Contributing
If you think that the library is missing something, feel free to make a pull request.	
There are [contribution guidelines](CONTRIBUTING.md "Contribution guidelines").

---

## Feedback, help, issues
If you need help with anything don't hesitate to open up an issue or message me directly.

---

<img src="https://sogomn.gitlab.io/spjgl/spjgl.svg" width="150">
