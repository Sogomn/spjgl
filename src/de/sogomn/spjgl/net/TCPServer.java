/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Simple TCP server.
 * @author Sogomn
 *
 */
public class TCPServer implements IClosable {
	
	private ServerSocket server;
	
	/**
	 * Constructs a new TCPServer object.
	 */
	public TCPServer() {
		//...
	}
	
	/**
	 * Constructs a new TCPServer object and binds the server to the given port.
	 * @param port The port the server should be bound to
	 */
	public TCPServer(final int port) {
		bind(port);
	}
	
	/**
	 * This method gets called when an exeption occurs. The default implementation prints the error and closes the server.
	 * @param ex The exception that has been thrown
	 */
	protected void handleException(final Exception ex) {
		if (ex instanceof NullPointerException) {
			System.err.println("Not bound: " + ex.getMessage());
		} else if (ex instanceof IOException) {
			System.err.println("Server closed: " + ex.getMessage());
		} else {
			System.err.println("Server error: " + ex.getMessage());
		}
		
		close();
	}
	
	/**
	 * Binds the server to the given port.
	 * @param port The port the server should be bound to
	 */
	public void bind(final int port) {
		try {
			server = new ServerSocket(port);
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Closes the server.
	 * This method does nothing if the server is not bound yet.
	 */
	@Override
	public void close() {
		if (server == null) {
			return;
		}
		
		try {
			server.close();
		} catch (final Exception ex) {
			//...
		}
		
		server = null;
	}
	
	/**
	 * Accepts the next incoming connection request.
	 * This will block the thread until a connection has been accepted or an exception has been thrown.
	 * Returns null if the server is not bound yet.
	 * @return The connection or null in case of failure
	 */
	public Socket acceptConnection() {
		if (server == null) {
			return null;
		}
		
		try {
			final Socket socket = server.accept();
			
			return socket;
		} catch (final Exception ex) {
			handleException(ex);
			
			return null;
		}
	}
	
	/**
	 * Rejects the next incoming connection request.
	 * This will block the thread until a connection has been accepted or an exception has been thrown.
	 * Does nothing if the server is not bound yet.
	 */
	public void rejectConnection() {
		if (server == null) {
			return;
		}
		
		try {
			final Socket socket = server.accept();
			
			socket.close();
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Returns the local port the server is bind to.
	 * @return The port or -1 if the server is not bound to a port
	 */
	public final int getPort() {
		if (server == null) {
			return -1;
		}
		
		return server.getLocalPort();
	}
	
	/**
	 * Returns true if the server is open and can accept connections, false otherwise.
	 * @return The state
	 */
	@Override
	public final boolean isOpen() {
		return server != null;
	}
	
}
