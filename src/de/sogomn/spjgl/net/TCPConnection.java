/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Communication between a client and a server.
 * @author Sogomn
 *
 */
public class TCPConnection extends AbstractTCPConnection {
	
	private DataInputStream in;
	private DataOutputStream out;
	
	/**
	 * Constructs a new TCPConnection object.
	 */
	public TCPConnection() {
		//...
	}
	
	@Override
	void initIO(final Socket socket) throws Exception {
		final InputStream inStream = socket.getInputStream();
		final OutputStream outStream = socket.getOutputStream();
		
		in = new DataInputStream(inStream);
		out = new DataOutputStream(outStream);
	}
	
	@Override
	void destroyIO() {
		try {
			in.close();
		} catch (final Exception ex) {
			//...
		} try {
			out.close();
		} catch (final Exception ex) {
			//...
		}
		
		in = null;
		out = null;
	}
	
	/**
	 * Prints an appropriate error message and closes the connection.
	 */
	@Override
	protected void handleException(final Exception ex) {
		if (ex instanceof NullPointerException) {
			System.err.println("Not connected: " + ex.getMessage());
		} else if (ex instanceof IOException) {
			System.err.println("Connection closed: " + ex.getMessage());
		} else {
			System.err.println("Connection error: " + ex.getMessage());
		}
		
		close();
	}
	
	/**
	 * Writes the data to the output stream.
	 * @param data The data to be sent
	 */
	public void write(final byte[] data) {
		try {
			out.write(data);
			out.flush();
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Writes an integer to the output stream.
	 * @param i The integer to be sent
	 */
	public void writeInt(final int i) {
		try {
			out.writeInt(i);
			out.flush();
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Writes a byte to the output stream.
	 * @param b The byte to be written
	 */
	public void writeByte(final byte b) {
		try {
			out.writeByte(b);
			out.flush();
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Writes a string in the modified UTF-8 format to the output stream.
	 * @param message The string to be written
	 */
	public void writeUTF(final String message) {
		try {
			out.writeUTF(message);
			out.flush();
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Writes a long to the output stream.
	 * @param l The long to be sent
	 */
	public void writeLong(final long l) {
		try {
			out.writeLong(l);
			out.flush();
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Writes a short to the output stream.
	 * @param s The short to be sent
	 */
	public void writeShort(final short s) {
		try {
			out.writeShort(s);
			out.flush();
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Writes a char to the output stream.
	 * @param c The char to be sent
	 */
	public void writeChar(final char c) {
		try {
			out.writeChar(c);
			out.flush();
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Reads data from the input stream and stores them into the given buffer.
	 * @param buffer The buffer the read data should be stored in
	 * @return The amount of bytes read or -1 if no bytes were read
	 */
	public int read(final byte[] buffer) {
		try {
			return in.read(buffer);
		} catch (final Exception ex) {
			handleException(ex);
			
			return -1;
		}
	}
	
	/**
	 * Reads an integer from the input stream and returns it.
	 * @return The integer read or zero in case of failure
	 */
	public int readInt() {
		try {
			return in.readInt();
		} catch (final Exception ex) {
			handleException(ex);
			
			return 0;
		}
	}
	
	/**
	 * Reads the next byte from the input stream and returns it.
	 * @return The next byte or zero in case of failure
	 */
	public byte readByte() {
		try {
			return in.readByte();
		} catch (final Exception ex) {
			handleException(ex);
			
			return 0;
		}
	}
	
	/**
	 * Reads a string in the modified UTF-8 format and returns it.
	 * @return The string or null in case of failure
	 */
	public String readUTF() {
		try {
			return in.readUTF();
		} catch (final Exception ex) {
			handleException(ex);
			
			return null;
		}
	}
	
	/**
	 * Reads the next long from the input stream and returns it.
	 * @return The next long or zero in case of failure
	 */
	public long readLong() {
		try {
			return in.readLong();
		} catch (final Exception ex) {
			handleException(ex);
			
			return 0;
		}
	}
	
	/**
	 * Reads the next short from the input stream and returns it.
	 * @return The next short or zero in case of failure
	 */
	public short readShort() {
		try {
			return in.readShort();
		} catch (final Exception ex) {
			handleException(ex);
			
			return 0;
		}
	}
	
	public char readChar() {
		try {
			return in.readChar();
		} catch (final Exception ex) {
			handleException(ex);
			
			return 0;
		}
	}
	
	/**
	 * Reads all the available bytes from the input stream.
	 * @return The read bytes or null in case of failure
	 */
	public byte[] readAllAvailable() {
		try {
			final int available = in.available();
			final byte[] buffer = new byte[available];
			
			in.read(buffer);
			
			return buffer;
		} catch (final Exception ex) {
			handleException(ex);
			
			return null;
		}
	}
	
	/**
	 * Returns the number of available bytes in the input stream.
	 * @return The available bytes
	 */
	public final int available() {
		try {
			return in.available();
		} catch (final Exception ex) {
			handleException(ex);
			
			return 0;
		}
	}
	
}
