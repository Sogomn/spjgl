/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.net;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * IRC message data class.
 * Use the "construct" method to parse a raw message to a more useful form.
 * @author Sogomn
 *
 */
public final class IRCMessage {
	
	/**
	 * IRC message part.
	 * May be null.
	 */
	public final String tags, serverOrNick, user, host, command, paramsMiddle, paramsTrail;
	
	/**
	 * A RegEx which can be used to parse the raw IRC messages.
	 * Used by the "construct" method.
	 * Yes, it was a paint to make.
	 */
	public static final Pattern MESSAGE_REGEX = Pattern.compile("^(?:@(.*?) )?(?::(.+?)(?:!(.+?))?(?:@(.+?))? )?((?:[A-z]+)|(?:[0-9]{3}))(?: (?!:)(.+?))?(?: :(.*))?$");
	
	private IRCMessage(final String tags, final String serverOrNick, final String user, final String host, final String command, final String paramsMiddle, final String paramsTrail) {
		this.tags = tags;
		this.serverOrNick = serverOrNick;
		this.user = user;
		this.host = host;
		this.command = command;
		this.paramsMiddle = paramsMiddle;
		this.paramsTrail = paramsTrail;
	}
	
	private IRCMessage() {
		this(null, null, null, null, null, null, null);
	}
	
	/**
	 * Returns the IRC message as a string.
	 * Simply joins all fields separated by space.
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		final String string = builder.append("[")
			.append(serverOrNick)
			.append(" ")
			.append(user)
			.append(" ")
			.append(host)
			.append(" ")
			.append(command)
			.append(" ")
			.append(paramsMiddle)
			.append(" ")
			.append(paramsTrail)
			.append("]")
			.toString();
		
		return string;
	}
	
	/**
	 * Constructs a new IRCMessage object from the given raw IRC message.
	 * If the raw message is not an IRC message then all fields of the returned object will be null.
	 * @param raw The raw message
	 * @return The parsed IRCMessage object or null if the raw message is null
	 */
	public static IRCMessage construct(final String raw) {
		if (raw == null) {
			return null;
		}
		
		final Matcher matcher = MESSAGE_REGEX.matcher(raw);
		final boolean found = matcher.find();
		
		if (!found) {
			return new IRCMessage();
		}
		
		final String tags = matcher.group(1);
		final String serverOrNick = matcher.group(2);
		final String user = matcher.group(3);
		final String host = matcher.group(4);
		final String command = matcher.group(5);
		final String paramsMiddle = matcher.group(6);
		final String paramsTrail = matcher.group(7);
		final IRCMessage message = new IRCMessage(tags, serverOrNick, user, host, command, paramsMiddle, paramsTrail);
		
		return message;
	}
	
}
