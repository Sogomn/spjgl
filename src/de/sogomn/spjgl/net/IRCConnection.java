/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.net;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.sogomn.spjgl.debug.Hardcoded;

/**
 * A connection which makes it easier to communicate with IRC servers.
 * There are no explicit checks whether the underlying connection is an actual IRC connection or not.
 * This class does not simplify all IRC commands (yet) but enough to create a simple bot or something else.
 * @author Sogomn
 *
 */
@Hardcoded("IRC commands are hardcoded as they most likely won't change. Creating a constant for every single one would be madness.")
public class IRCConnection extends AbstractTCPConnection {
	
	/*
	 * "Middles" means the middle parameters
	 * "Trailing" is the trailing parameter
	 * The basic IRC command format is "COMMAND MIDDLE MIDDLE MIDDLE :TRAILING"
	 * This allows the trailing parameter to contain spaces
	 * The number of middle parameters is variable
	 * Check the IRC RFC for more info
	 */
	
	private BufferedReader reader;
	private BufferedWriter writer;
	
	private static final String NEW_LINE = "\r\n";
	private static final int DEFAULT_PORT = 6667;
	private static final String COMMAND_FORMAT = "%s %s :%s";
	private static final String COMMAND_FORMAT_NO_TRAILING = "%s %s";
	private static final String COMMAND_FORMAT_NO_MIDDLES = "%s :%s";
	
	/**
	 * Creates a new IRCConnection object.
	 */
	public IRCConnection() {
		//...
	}
	
	@Override
	void initIO(final Socket socket) throws Exception {
		final InputStream in = socket.getInputStream();
		final OutputStream out = socket.getOutputStream();
		final InputStreamReader inReader = new InputStreamReader(in);
		final OutputStreamWriter outWriter = new OutputStreamWriter(out);
		
		reader = new BufferedReader(inReader);
		writer = new BufferedWriter(outWriter);
	}
	
	@Override
	void destroyIO() {
		try {
			writer.close();
		} catch (final Exception ex) {
			//...
		} try {
			reader.close();
		} catch (final Exception ex) {
			//...
		}
		
		writer = null;
		reader = null;
	}
	
	/**
	 * Prints the error to <code>System.err</code> and closes the connection.
	 */
	@Override
	protected void handleException(final Exception ex) {
		System.err.println(ex);
		
		close();
	}
	
	/**
	 * Reads a raw line from the input stream.
	 * @return The line
	 */
	protected String readLine() {
		try {
			return reader.readLine();
		} catch (final Exception ex) {
			handleException(ex);
			
			return null;
		}
	}
	
	/**
	 * Writes a raw line to the output stream.
	 * @param line The line to send
	 */
	protected void writeLine(final String line) {
		try {
			writer.write(line + NEW_LINE);
			writer.flush();
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Connects to the specified address and the default IRC port (6667).
	 * @param address The address
	 */
	public final void connect(final String address) {
		connect(address, DEFAULT_PORT);
	}
	
	/**
	 * Reads the next message from the IRC server.
	 * This method is blocking.
	 * @return The read message
	 */
	public IRCMessage readMessage() {
		final String raw = readLine();
		final IRCMessage message = IRCMessage.construct(raw);
		
		return message;
	}
	
	/**
	 * Sends a command with parameters to the IRC server.
	 * @param command The command
	 * @param middles The middle parameters
	 * @param trailing The trailing parameter
	 */
	public void sendCommand(final String command, final String[] middles, final String trailing) {
		final Collector<CharSequence, ?, String> joining = Collectors.joining(" ");
		final String middleString = Stream.of(middles)
			.collect(joining);
		final String message = String.format(COMMAND_FORMAT, command, middleString, trailing);
		
		writeLine(message);
	}
	
	/**
	 * Sends a command with parameters to the IRC server.
	 * Does not send a trailing parameter.
	 * @param command The command
	 * @param middles The middle parameters
	 */
	public void sendCommand(final String command, final String[] middles) {
		final Collector<CharSequence, ?, String> joining = Collectors.joining(" ");
		final String middleString = Stream.of(middles)
			.collect(joining);
		final String message = String.format(COMMAND_FORMAT_NO_TRAILING, command, middleString);
		
		writeLine(message);
	}
	
	/**
	 * Sends a command and a trailing parameter to the IRC server.
	 * @param command The command
	 * @param trailing The trailing parameter
	 */
	public void sendCommand(final String command, final String trailing) {
		final String message = String.format(COMMAND_FORMAT_NO_MIDDLES, command, trailing);
		
		writeLine(message);
	}
	
	/**
	 * Sends a command to the IRC server.
	 * @param command The command
	 */
	public void sendCommand(final String command) {
		writeLine(command);
	}
	
	/**
	 * Logs in to the IRC server. This should be called immediately after a connection has been established.
	 * @param nick The nickname
	 * @param pass The password
	 */
	public void logIn(final String nick, final String pass) {
		final String[] nickMiddles = {nick};
		final String[] passMiddles = {pass};
		final String[] userMiddles = {"usr", "0", "*"};
		
		sendCommand("PASS", passMiddles);
		sendCommand("NICK", nickMiddles);
		sendCommand("USER", userMiddles, nick);
	}
	
	/**
	 * Logs in to the IRC server. This should be called immediately after a connection has been established.
	 * @param nick The nickname
	 */
	public void logIn(final String nick) {
		final String[] nickMiddles = {nick};
		final String[] userMiddles = {"usr", "0", "*"};
		
		sendCommand("NICK", nickMiddles);
		sendCommand("USER", userMiddles, nick);
	}
	
	/**
	 * Changes the nickname.
	 * Sends a NICK message.
	 * @param nick The nickname
	 */
	public void nick(final String nick) {
		final String[] nickArray = {nick};
		
		sendCommand("NICK", nickArray);
	}
	
	/**
	 * Quits from the server. The connection will most likely be closed.
	 * Sends a QUIT message.
	 */
	public void quit() {
		sendCommand("QUIT");
	}
	
	/**
	 * Quits from the server. The connection will most likely be closed.
	 * Sends a QUIT message with the given message as a parameter.
	 * @param message The message parameter
	 */
	public void quit(final String message) {
		sendCommand("QUIT", message);
	}
	
	/**
	 * Joins one or more channels.
	 * Sends a JOIN message.
	 * @param channels The channels to join separated by space
	 */
	public void join(final String... channels) {
		final String[] middles = joinAllBy(",", channels);
		
		sendCommand("JOIN", middles);
	}
	
	/**
	 * Joins one or multiple channels and provides the given keys (passwords). Those can be empty.
	 * Sends a JOIN message.
	 * @param channels The channels
	 * @param keys The keys
	 */
	public void join(final String[] channels, final String[] keys) {
		final String[] middles = joinAllBy(",", channels, keys);
		
		sendCommand("JOIN", middles);
	}
	
	/**
	 * Leaves one or more channels.
	 * Sends a PART message.
	 * @param channels The channels to leave
	 */
	public void part(final String... channels) {
		sendCommand("PART", channels);
	}
	
	/**
	 * Sends a LIST message to the server.
	 * Usually lists the channels and their topics.
	 */
	public void list() {
		sendCommand("LIST");
	}
	
	/**
	 * Sends a LIST message to the server with the given channels as a parameter.
	 * Usually lists the channel topics.
	 * @param channels The channels to list
	 */
	public void list(final String... channels) {
		sendCommand("LIST", channels);
	}
	
	/**
	 * Sends the given message to the given receivers.
	 * Sends a PRIVMSG message.
	 * @param receivers The receivers; e.g. a channel or a user
	 * @param message The message to send
	 */
	public void privMsg(final String receivers, final String message) {
		final String[] middles = {receivers};
		
		sendCommand("PRIVMSG", middles, message);
	}
	
	/**
	 * Sends the given message to the given receivers.
	 * Sends a NOTICE message.
	 * NOTICE messages are the same as PRIVMSG except that they should not be automatically replied to.
	 * @param receivers The receivers; e.g. a channel or a user
	 * @param message The message to send
	 */
	public void notice(final String receivers, final String message) {
		final String[] middles = {receivers};
		
		sendCommand("NOTICE", middles, message);
	}
	
	/**
	 * Sends a PONG message.
	 * Should be the reply to a server's PING message.
	 */
	public void pong() {
		sendCommand("PONG");
	}
	
	/*
	 * Turns all arrays to strings separated by the specified sequence and puts them into a new array.
	 * So joinAllBy(",", { "a", "b", "c" }, { "x", "y", "z" }) returns { "a,b,c", "x,y,z" }.
	 */
	private static String[] joinAllBy(final String sequence, final String[]... arrays) {
		final Collector<CharSequence, ?, String> joining = Collectors.joining(sequence);
		final String[] result = new String[arrays.length];
		
		for (int i = 0; i < arrays.length; i++) {
			final String[] array = arrays[i];
			final String arrayString = Stream.of(array)
				.collect(joining);
			
			result[i] = arrayString;
		}
		
		return result;
	}
	
}
