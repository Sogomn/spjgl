/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.net;

import java.net.InetAddress;
import java.net.Socket;

abstract class AbstractTCPConnection implements IClosable {
	
	private Socket socket;
	
	public AbstractTCPConnection() {
		//...
	}
	
	abstract void initIO(final Socket socket) throws Exception;
	
	abstract void destroyIO();
	
	/**
	 * This method gets called when an exeption occurs.
	 * @param ex The exception that has been thrown
	 */
	protected abstract void handleException(final Exception ex);
	
	/**
	 * Connects to the specified address and port.
	 * Does nothing if there already is a connection.
	 * @param address The target address
	 * @param port The target port
	 */
	public final void connect(final String address, final int port) {
		if (socket != null) {
			return;
		}
		
		try {
			socket = new Socket(address, port);
			
			initIO(socket);
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Uses the given socket to establish a connection.
	 * Does nothing if there already is a connection.
	 * @param socket The socket to be used
	 */
	public final void connect(final Socket socket) {
		if (socket != null) {
			return;
		}
		
		this.socket = socket;
		
		try {
			initIO(socket);
		} catch (final Exception ex) {
			handleException(ex);
		}
	}
	
	/**
	 * Closes the connection and all its streams.
	 * This method does nothing if the connection is not open.
	 */
	@Override
	public final void close() {
		if (socket == null) {
			return;
		}
		
		destroyIO();
		
		try {
			socket.close();
		} catch (final Exception ex) {
			//...
		}
		
		socket = null;
	}
	
	/**
	 * Returns true if the connection is open, false otherwise.
	 * @return The state
	 */
	@Override
	public final boolean isOpen() {
		return socket != null;
	}
	
	/**
	 * Returns the remote host address.
	 * @return The address or null if there is no connection
	 */
	public final String getAddress() {
		if (socket == null) {
			return null;
		}
		
		final InetAddress address = socket.getInetAddress();
		
		return address.getHostAddress();
	}
	
	/**
	 * Returns the remote port.
	 * @return The port or -1 if there is no connection
	 */
	public final int getPort() {
		if (socket == null) {
			return -1;
		}
		
		return socket.getPort();
	}
	
	/**
	 * Returns the local port.
	 * @return The local port or -1 if there is no connection
	 */
	public final int getLocalPort() {
		if (socket == null) {
			return -1;
		}
		
		return socket.getLocalPort();
	}
	
}
