/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.fx;

/**
 * Interface for sound listeners to be notified when a clip stops or finishes a loop.
 * @author Sogomn
 *
 */
@FunctionalInterface
public interface ISoundListener {
	
	/**
	 * Called when a sound finished one loop.
	 * @param source The calling Sound object
	 */
	void looped(final Sound source);
	
	/**
	 * Called when a sound stops.
	 * This method has a default implementation to enable functional usage of this class.
	 * @param source The calling Sound object
	 */
	default void stopped(final Sound source) {
		//...
	}
	
}
