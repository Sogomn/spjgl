/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.fx;

import java.awt.image.BufferedImage;

import de.sogomn.spjgl.util.ImageUtils;

/**
 * Sprite sheet management.
 * @author Sogomn
 *
 */
public final class SpriteSheet {
	
	private BufferedImage[][] sprites;
	private int spriteWidth, spriteHeight;
	private int spritesWide, spritesHigh;
	
	/**
	 * Constructs a SpriteSheet object from the given image and the sprite dimensions.
	 * @param image The base image
	 * @param spriteWidth The width of every sprite
	 * @param spriteHeight The height of every sprite
	 */
	public SpriteSheet(final BufferedImage image, final int spriteWidth, final int spriteHeight) {
		this.spriteWidth = spriteWidth;
		this.spriteHeight = spriteHeight;
		
		final int width = image.getWidth();
		final int height = image.getHeight();
		
		spritesWide = width / spriteWidth;
		spritesHigh = height / spriteHeight;
		sprites = new BufferedImage[spritesWide][spritesHigh];
		
		loadSprites(image);
	}
	
	/**
	 * Constructs a SpriteSheet object from the given path (classpath!) and sprite dimensions.
	 * @param path The path to the base image
	 * @param spriteWidth The width of every sprite
	 * @param spriteHeight The height of every sprite
	 */
	public SpriteSheet(final String path, final int spriteWidth, final int spriteHeight) {
		this(ImageUtils.load(path), spriteWidth, spriteHeight);
	}
	
	private void loadSprites(final BufferedImage image) {
		for (int x = 0; x < spritesWide; x++) {
			for (int y = 0; y < spritesHigh; y++) {
				final int imageX = x * spriteWidth;
				final int imageY = y * spriteHeight;
				final BufferedImage subimage = image.getSubimage(imageX, imageY, spriteWidth, spriteHeight);
				
				sprites[x][y] = subimage;
			}
		}
	}
	
	/**
	 * Returns the sprite at the given row (y) and column (x).
	 * @param x The x index (column)
	 * @param y The y index (row)
	 * @return The sprite as a BufferedImage or null if the indices are out of bounds
	 */
	public BufferedImage getSprite(final int x, final int y) {
		if (x < 0 || y < 0 || x > spritesWide - 1 || y > spritesHigh - 1) {
			return null;
		}
		
		final BufferedImage image = sprites[x][y];
		
		return image;
	}
	
	/**
	 * Returns the sprite at the given index.
	 * @param orientation The orientation (left to right or top to bottom)
	 * @param index The index
	 * @return The sprite as a BufferedImage or null if the index is out of bounds
	 */
	public BufferedImage getSprite(final Orientation orientation, final int index) {
		int x = 0;
		int y = 0;
		
		if (orientation == Orientation.ROW_MAJOR) {
			x = index % spritesWide;
			y = index / spritesWide;
		} else if (orientation == Orientation.COLUMN_MAJOR) {
			x = index / spritesWide;
			y = index % spritesWide;
		}
		
		final BufferedImage image = getSprite(x, y);
		
		return image;
	}
	
	/**
	 * Returns the sprite at the given index and left to right orientation.
	 * @param index The index
	 * @return The sprite as a BufferedImage or null if the index is out of bounds
	 */
	public BufferedImage getSprite(final int index) {
		return getSprite(Orientation.ROW_MAJOR, index);
	}
	
	/**
	 * Returns the sprites at the given indices as an array.
	 * @param orientation The orientation (left to right or top to bottom)
	 * @param indices The indices
	 * @return The sprites as an array
	 */
	public BufferedImage[] getSprites(final Orientation orientation, final int... indices) {
		final BufferedImage[] images = new BufferedImage[indices.length];
		
		for (int i = 0; i < indices.length; i++) {
			final int index = indices[i];
			
			images[i] = getSprite(orientation, index);
		}
		
		return images;
	}
	
	/**
	 * Returns the sprites at the given indices as an array.
	 * The orientation is left to right.
	 * @param indices The indices
	 * @return The sprites as an array
	 */
	public BufferedImage[] getSprites(final int... indices) {
		return getSprites(Orientation.ROW_MAJOR, indices);
	}
	
	/**
	 * Returns all sprites between and at the given indices.
	 * @param orientation The orientation (left to right or top to bottom)
	 * @param from The start index (inclusive)
	 * @param to The end index (inclusive)
	 * @return The sprites as an array
	 */
	public BufferedImage[] getSpritesBetween(final Orientation orientation, final int from, final int to) {
		final int length = to - from + 1;
		final BufferedImage[] images = new BufferedImage[length];
		
		for (int i = 0; i < length; i++) {
			final int index = from + i;
			final BufferedImage image = getSprite(orientation, index);
			
			images[i] = image;
		}
		
		return images;
	}
	
	/**
	 * Returns all sprites between and at the given indices.
	 * The orientation is left to right.
	 * @param from The start index (inclusive)
	 * @param to The end index (inclusive)
	 * @return The sprites as an array
	 */
	public BufferedImage[] getSpritesBetween(final int from, final int to) {
		return getSprites(Orientation.ROW_MAJOR, from, to);
	}
	
	/**
	 * Returns all sprites this sprite sheet holds.
	 * @return The sprites as an array
	 */
	public BufferedImage[] getSprites() {
		final BufferedImage[] images = new BufferedImage[spritesWide * spritesHigh];
		
		for (int x = 0; x < spritesWide; x++) {
			for (int y = 0; y < spritesHigh; y++) {
				final BufferedImage image = getSprite(x, y);
				final int index = x + y * spritesWide;
				
				images[index] = image;
			}
		}
		
		return images;
	}
	
	/**
	 * Returns the amount of sprites in the sprite sheet.
	 * @return The sprite count
	 */
	public int spriteCount() {
		return spritesWide * spritesHigh;
	}
	
	/**
	 * Returns the width of each sprite.
	 * @return The width
	 */
	public int getSpriteWidth() {
		return spriteWidth;
	}
	
	/**
	 * Returns the height of each sprite.
	 * @return The height
	 */
	public int getSpriteHeight() {
		return spriteHeight;
	}
	
	/**
	 * Returns the amount of sprites per row.
	 * @return The sprites per row
	 */
	public int getSpritesWide() {
		return spritesWide;
	}
	
	/**
	 * Returns the amount of sprites per column.
	 * @return The sprites per column
	 */
	public int getSpritesHigh() {
		return spritesHigh;
	}
	
	/**
	 * Holds the different orientations for the SpriteSheet class.
	 * @author Sogomn
	 *
	 */
	public enum Orientation {
		
		/**
		 * Row major (left-to-right) orientation.
		 */
		ROW_MAJOR,
		
		/**
		 * Column major (top-to-bottom) orientation.
		 */
		COLUMN_MAJOR;
		
	}
	
}
