/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.fx;

/**
 * Interface for an animation listener which can react to animation loops.
 * @author Sogomn
 *
 */
@FunctionalInterface
public interface IAnimationListener {
	
	/**
	 * Called when an animation loop has finished.
	 * @param source The calling Animation object
	 */
	void looped(final Animation source);
	
	/**
	 * Called when an animation has reached the max loop count.
	 * This method has a default implementation to enable functional usage of this class.
	 * @param source The calling Animation object
	 */
	default void stopped(final Animation source) {
		//...
	}
	
}
