/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.fx;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import de.sogomn.spjgl.Clock;
import de.sogomn.spjgl.IUpdatable;
import de.sogomn.spjgl.util.Scheduler;
import de.sogomn.spjgl.util.Scheduler.Task;

/**
 * Game camera transform class.
 * In order to apply the translation the method {@link #apply(Graphics2D)} needs to be called before rendering. It also needs to get updated every tick.
 * After rendering, the method {@link #revert(Graphics2D)} should be called to undo the translation.
 * @author Sogomn
 *
 */
public final class Camera implements IUpdatable {
	
	private double x, y;
	private double targetX, targetY;
	private double smoothness;
	
	private double minX, minY;
	private double maxX, maxY;
	
	private double rotation;
	private double pivotX, pivotY;
	
	private double zoom;
	private double centerX, centerY;
	
	private double offsetX, offsetY;
	private double rotationOffset;
	
	private Scheduler shakeScheduler;
	private Shaker shaker;
	
	private AffineTransform oldTransform;
	
	/**
	 * The camera position will automatically be the target position.
	 */
	public static final double NO_SMOOTHNESS = 0.0;
	
	/**
	 * Represents a minimum value of no fixed size.
	 */
	public static final int NO_MINIMUM = Integer.MIN_VALUE;
	
	/**
	 * Represents a maximum value of no fixed size.
	 */
	public static final int NO_MAXIMUM = Integer.MAX_VALUE;
	
	/**
	 * No camera zoom.
	 * This is default.
	 */
	public static final double NO_ZOOM = 1.0;
	
	/**
	 * Constructs a new Camera object pointing at the given location with a smoothness of 0 and no minimum or maximum values.
	 * @param x The initial x position of the camera
	 * @param y The initial y position of the camera
	 */
	public Camera(final double x, final double y) {
		this.x = targetX = x;
		this.y = targetY = y;
		
		smoothness = NO_SMOOTHNESS;
		minX = minY = NO_MINIMUM;
		maxX = maxY = NO_MAXIMUM;
		zoom = NO_ZOOM;
		shakeScheduler = new Scheduler();
		shaker = new Shaker();
		oldTransform = new AffineTransform();
	}
	
	/**
	 * Constructs a new Camera object at (0, 0) with a smoothness of 0 and no minimum or maximum values.
	 */
	public Camera() {
		this(0, 0);
	}
	
	private void move(final long delta) {
		if (smoothness == NO_SMOOTHNESS) {
			x = targetX;
			y = targetY;
		} else {
			final double distX = targetX - x;
			final double distY = targetY - y;
			final double deltaSeconds = Clock.asSeconds(delta);
			
			x += distX * deltaSeconds / smoothness;
			y += distY * deltaSeconds / smoothness;
		}
	}
	
	private void clampPosition() {
		x = Math.max(Math.min(x, maxX), minX);
		y = Math.max(Math.min(y, maxY), minY);
		
		final double actualX = getX();
		final double actualY = getY();
		
		if (actualX < minX) {
			offsetX = x - minX;
		} else if (actualX > maxX) {
			offsetX = x - maxX;
		}
		
		if (actualY < minY) {
			offsetY = y - minY;
		} else if (actualY > maxY) {
			offsetY = y - maxY;
		}
	}
	
	private void clampTarget() {
		targetX = Math.max(Math.min(targetX, maxX), minX);
		targetY = Math.max(Math.min(targetY, maxY), minY);
	}
	
	/**
	 * Updates the camera.
	 */
	@Override
	public void update(final long delta) {
		move(delta);
		
		shakeScheduler.update(delta);
		shaker.update(delta);
		
		clampPosition();
	}
	
	/**
	 * Applies the camera transform to the given Graphics2D object.
	 * @param g The Graphics2D object
	 */
	public void apply(final Graphics2D g) {
		final double actualX = getX();
		final double actualY = getY();
		final double actualRotation = getRotation();
		final AffineTransform transform = new AffineTransform();
		
		oldTransform = g.getTransform();
		
		transform.rotate(actualRotation, pivotX, pivotY);
		transform.translate(-actualX, -actualY);
		transform.translate(centerX, centerY);
		transform.scale(zoom, zoom);
		transform.translate(-centerX, -centerY);
		
		g.transform(transform);
	}
	
	/**
	 * Reverts the camera tramsform of the given Graphics2D object.
	 * This should only be called after the method "apply" has been called.
	 * @param g The Graphics2D object
	 */
	public void revert(final Graphics2D g) {
		g.setTransform(oldTransform);
	}
	
	/**
	 * Resets the camera position and target.
	 * Also stops camera shake.
	 */
	public void reset() {
		x = y = 0;
		targetX = targetY = 0;
		
		resetShake();
		resetRotation();
		resetZoom();
	}
	
	/**
	 * Stops and resets the camera shake.
	 */
	public void resetShake() {
		offsetX = offsetY = 0;
		rotationOffset = 0;
		
		shakeScheduler.removeAllTasks();
		shaker.stop();
		
		shaker = new Shaker();
	}
	
	/**
	 * Resets the rotation and the rotation pivot.
	 */
	public void resetRotation() {
		rotation = 0;
		pivotX = pivotY = 0;
	}
	
	/**
	 * Resets the zoom and the scale center point.
	 */
	public void resetZoom() {
		zoom = 0;
		centerX = centerY = 0;
	}
	
	/**
	 * Applies camera shake for the given duration with the given intensity.
	 * @param intensityX The intensity on the x-axis
	 * @param intensityY The intensity on the y-axis
	 * @param intensityRotation The rotation shake intensity in degrees
	 * @param duration The duration in nanoseconds
	 */
	public void shake(final double intensityX, final double intensityY, final double intensityRotation, final long duration) {
		final Task task = new Task(duration, () -> {
			resetShake();
			
			return false;
		});
		
		resetShake();
		
		shakeScheduler.addTask(task);
		shaker = new Shaker(intensityX, intensityY, Math.toRadians(intensityRotation), duration);
	}
	
	/**
	 * Offsets the target position of the camera by the given one.
	 * @param x The x coordinate
	 * @param y The y coordinate
	 */
	public void moveBy(final double x, final double y) {
		targetX += x;
		targetY += y;
		
		clampTarget();
	}
	
	/**
	 * Sets the target position of the camera to the given one.
	 * @param x The x coordinate
	 * @param y The y coordinate
	 */
	public void moveTo(final double x, final double y) {
		targetX = x;
		targetY = y;
		
		clampTarget();
	}
	
	/**
	 * Sets the camera and the target position to the given one.
	 * @param x The x coordinate
	 * @param y The y coordinate
	 */
	public void set(final double x, final double y) {
		this.x = targetX = x;
		this.y = targetY = y;
		
		clampTarget();
		clampPosition();
	}
	
	/**
	 * Sets the minimum position the camera can have.
	 * @param minX The minimum x value
	 * @param minY The minimum y value
	 */
	public void setMinimum(final double minX, final double minY) {
		this.minX = minX;
		this.minY = minY;
	}
	
	/**
	 * Sets the maximum position the camera can have.
	 * @param maxX The maximum x value
	 * @param maxY The maximum y value
	 */
	public void setMaximum(final double maxX, final double maxY) {
		this.maxX = maxX;
		this.maxY = maxY;
	}
	
	/**
	 * Sets the minimum and maximum values the camera can have.
	 * @param minX The minimum x value
	 * @param minY The minimum y value
	 * @param maxX The maximum x value
	 * @param maxY The maximum y value
	 */
	public void setBounds(final double minX, final double minY, final double maxX, final double maxY) {
		setMinimum(minX, minY);
		setMaximum(maxX, maxY);
	}
	
	/**
	 * Sets the rotation of the camera.
	 * @param degrees The rotation in degrees
	 */
	public void setRotation(final double degrees) {
		rotation = Math.toRadians(degrees);
	}
	
	/**
	 * Sets the pivot for the camera rotation.
	 * The default is (0|0)
	 * @param pivotX The x coordinate
	 * @param pivotY The y coordinate
	 */
	public void setRotationPivot(final double pivotX, final double pivotY) {
		this.pivotX = pivotX;
		this.pivotY = pivotY;
	}
	
	/**
	 * Sets the zoom.
	 * No zoom is default.
	 * @param zoom The zoom value
	 */
	public void setZoom(final double zoom) {
		this.zoom = zoom;
	}
	
	/**
	 * Sets the zoom center point.
	 * @param centerX The x coordinate
	 * @param centerY The y coordinate
	 */
	public void setZoomCenterPoint(final double centerX, final double centerY) {
		this.centerX = centerX;
		this.centerY = centerY;
	}
	
	/**
	 * Sets the smoothness for the camera.
	 * Good values are between 0 an 1.
	 * @param smoothness The smoothness
	 */
	public void setSmoothness(final double smoothness) {
		this.smoothness = smoothness;
	}
	
	/**
	 * Returns the x coordinate of the camera.
	 * Ignores zoom.
	 * @return The coordinate
	 */
	public double getX() {
		return x + offsetX;
	}
	
	/**
	 * Returns the y coordinate of the camera.
	 * Ignores zoom.
	 * @return The coordinate
	 */
	public double getY() {
		return y + offsetY;
	}
	
	/**
	 * Returns the target x coordinate of the camera.
	 * @return The coordinate
	 */
	public double getTargetX() {
		return targetX;
	}
	
	/**
	 * Returns the target y coordinate of the camera.
	 * @return The coordinate
	 */
	public double getTargetY() {
		return targetY;
	}
	
	/**
	 * Returns the scrolling smoothness.
	 * @return The smoothness
	 */
	public double getSmoothness() {
		return smoothness;
	}
	
	/**
	 * Returns the minimum x value.
	 * @return The value
	 */
	public double getMinimumX() {
		return minX;
	}
	
	/**
	 * Returns the minimum y value.
	 * @return The value
	 */
	public double getMinimumY() {
		return minY;
	}
	
	/**
	 * Returns the maximum x value.
	 * @return The value
	 */
	public double getMaximumX() {
		return maxX;
	}
	
	/**
	 * Returns the maximum y value.
	 * @return The value
	 */
	public double getMaximumY() {
		return maxY;
	}
	
	/**
	 * Returns the camera rotation.
	 * Includes the rotation shake.
	 * @return The rotation
	 */
	public double getRotation() {
		return rotation + rotationOffset;
	}
	
	/**
	 * Returns the zoom.
	 * @return The zoom value
	 */
	public double getZoom() {
		return zoom;
	}
	
	/**
	 * Returns whether the camera is shaking or not.
	 * @return The state
	 */
	public boolean isShaking() {
		return shaker.shaking;
	}
	
	private final class Shaker implements IUpdatable {
		
		private boolean shaking;
		private double intensityX, intensityY;
		private double intensityRotation;
		private long duration;
		
		public Shaker() {
			//...
		}
		
		public Shaker(final double intensityX, final double intensityY, final double intensityRotation, final long duration) {
			this.intensityX = intensityX;
			this.intensityY = intensityY;
			this.intensityRotation = intensityRotation;
			this.duration = Math.max(duration, 0);
			
			shaking = true;
		}
		
		@Override
		public void update(final long delta) {
			if (!shaking) {
				return;
			}
			
			final double step = (double)delta / duration;
			
			/*Modifying the Camera class variables*/
			offsetX = Math.random() * intensityX * 2 - intensityX;
			offsetY = Math.random() * intensityY * 2 - intensityY;
			rotationOffset = Math.random() * intensityRotation * 2 - intensityRotation;
			
			intensityX -= intensityX * step;
			intensityY -= intensityY * step;
			intensityRotation -= intensityRotation * step;
		}
		
		public void stop() {
			shaking = false;
		}
		
	}
	
}
