/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.noise;

import java.util.Random;

import de.sogomn.spjgl.debug.Hardcoded;
import de.sogomn.spjgl.math.Vector2;

/**
 * Perlin noise.
 * Kind of like <a href="http://mrl.nyu.edu/~perlin/noise/">the original</a> but easier to read and less efficient.
 * Generates values from 1 to -1.
 * The noise value at integer coordinates will most likely be 0.
 * @author Sogomn
 *
 */
public final class PerlinNoise extends AbstractGridNoise {
	
	private double contrast;
	
	private static final double DEFAULT_CONTRAST = 1.0;
	
	/**
	 * Constructs a new PerlinNoise object.
	 * @param seedX The seed for the x-axis permutation
	 * @param seedY The seed for the y-axis permutation
	 */
	public PerlinNoise(final long seedX, final long seedY) {
		super(seedX, seedY);
		
		contrast = DEFAULT_CONTRAST;
	}
	
	private Vector2 gridVector(final int x, final int y) {
		final int hash = hash(x, y);
		final long seed = Integer.toUnsignedLong(hash);
		final Random ran = new Random(seed);
		final int vectorX = ran.nextInt();
		final int vectorY = ran.nextInt();
		final Vector2 vector = new Vector2(vectorX, vectorY)
			.normalize();
		
		return vector;
	}
	
	/**
	 * Returns the noise value at the given point.
	 * The value is the relation of the nearest four grid vectors and the distance of the given point to those.
	 * @return A value from 1 to -1 (inclusive)
	 */
	@Override
	public double getValue(final double x, final double y) {
		/*Grid coordinates*/
		final int x0 = (int)Math.floor(x);
		final int y0 = (int)Math.floor(y);
		final int x1 = x0 + 1;
		final int y1 = y0 + 1;
		
		/*Points*/
		final Vector2 samplePoint = new Vector2(x, y);
		final Vector2 gridPoint00 = new Vector2(x0, y0);
		final Vector2 gridPoint10 = new Vector2(x1, y0);
		final Vector2 gridPoint01 = new Vector2(x0, y1);
		final Vector2 gridPoint11 = new Vector2(x1, y1);
		
		/*Distances from point to grid point*/
		final Vector2 distance00 = gridPoint00.subtract(samplePoint);
		final Vector2 distance10 = gridPoint10.subtract(samplePoint);
		final Vector2 distance01 = gridPoint01.subtract(samplePoint);
		final Vector2 distance11 = gridPoint11.subtract(samplePoint);
		
		/*Grid vectors*/
		final Vector2 grid00 = gridVector(x0, y0);
		final Vector2 grid10 = gridVector(x1, y0);
		final Vector2 grid01 = gridVector(x0, y1);
		final Vector2 grid11 = gridVector(x1, y1);
		
		/*Direction relations of distances and grid vectors*/
		final double dot00 = distance00.dot(grid00);
		final double dot10 = distance10.dot(grid10);
		final double dot01 = distance01.dot(grid01);
		final double dot11 = distance11.dot(grid11);
		
		/*Interpolation weights*/
		final double weightX = advancedWeight(x - x0);
		final double weightY = advancedWeight(y - y0);
		
		/*Interpolation of the relations*/
		final double top = interpolate(dot00, dot10, weightX);
		final double bottom = interpolate(dot01, dot11, weightX);
		
		double value = interpolate(top, bottom, weightY) * contrast;
		value = Math.max(Math.min(value, 1), -1);//Just to be safe
		
		return value;
	}
	
	/**
	 * Sets the contrast for the noise.
	 * @param contrast The new contrast value
	 */
	public void setContrast(final double contrast) {
		this.contrast = contrast;
	}
	
	/**
	 * Returns the currently set contrast.
	 * Default is 1.
	 * @return The contrast
	 */
	public double getContrast() {
		return contrast;
	}
	
	/*This is the "fade" function*/
	@Hardcoded
	private static double advancedWeight(final double weight) {
		return weight * weight * weight * (weight * (weight * 6 - 15) + 10);
	}
	
	private static double interpolate(final double one, final double two, final double weight) {
		final double value = one * (1 - weight) + two * weight;
		
		return value;
	}
	
}
