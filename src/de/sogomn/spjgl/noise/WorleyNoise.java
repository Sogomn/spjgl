/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.noise;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;

import de.sogomn.spjgl.debug.Hardcoded;
import de.sogomn.spjgl.math.Vector2;

/**
 * Worley noise.
 * <a href="http://www.rhythmiccanvas.com/research/papers/worley.pdf">This paper</a> explains it.
 * It is definitely not optimized for efficiency. This implementation is slow! Really slow!
 * You can set your own functions for distance and value calculation. The default implementations sample the squared distance to the second closest feature point.
 * @author Sogomn
 *
 */
public final class WorleyNoise extends AbstractGridNoise {
	
	private int density;
	private BiFunction<Vector2, Vector2, Double> distanceFunction;
	private Function<List<Double>, Double> valueFunction;
	
	private static final int DEFAULT_DENSITY = 3;
	private static final int MIN_FEATURE_POINTS_USED = 3;
	private static final String MESSAGE_NEGATIVE_PARAMETER = "Parameter must be positive";
	
	public static final BiFunction<Vector2, Vector2, Double> DEFAULT_DISTANCE_FUNCTION = (one, two) -> two.subtract(one)
		.lengthSquared();
	public static final Function<List<Double>, Double> DEFAULT_VALUE_FUNCTION = list -> list.get(1);
	
	/**
	 * Creates a new WorleyNoise object with the specified feature point density.
	 * @param seedX The seed for the x-axis permutation
	 * @param seedY The seed for the y-axis permutation
	 * @param density The feature point density
	 */
	public WorleyNoise(final long seedX, final long seedY, final int density) {
		super(seedX, seedY);
		
		this.density = density;
		
		distanceFunction = DEFAULT_DISTANCE_FUNCTION;
		valueFunction = DEFAULT_VALUE_FUNCTION;
	}
	
	/**
	 * Creates a new WorleyNoise object.
	 * Default values for the density and point index are 3 and 1.
	 * @param seedX The seed for the x-axis permutation
	 * @param seedY The seed for the y-axis permutation
	 */
	public WorleyNoise(final long seedX, final long seedY) {
		this(seedX, seedY, DEFAULT_DENSITY);
	}
	
	/*Returns the k for the poisson distribution value closest to the parameter*/
	private int featurePointCount(final double probability) {
		for (int i = density; i >= 0; i--) {
			final double value = poisson(density, i);
			
			if (value < probability) {
				return i;
			}
		}
		
		return 1;
	}
	
	/*Gets the ordered squared feature point distances of the specified location and puts them into the list*/
	private void featurePointDistances(final int quadX, final int quadY, final double x, final double y, final ArrayList<Double> pointList) {
		final int hash = hash(quadX, quadY);
		final long seed = Integer.toUnsignedLong(hash);
		final Random ran = new Random(seed);
		final double probability = ran.nextDouble() / density;
		final int pointCount = featurePointCount(probability);
		final Vector2 samplePoint = new Vector2(x, y);
		
		for (int i = 0; i < pointCount; i++) {
			final double pointX = ran.nextDouble() + quadX;
			final double pointY = ran.nextDouble() + quadY;
			final Vector2 featurePoint = new Vector2(pointX, pointY);
			final double distance = distanceFunction.apply(samplePoint, featurePoint);
			
			insertSorted(pointList, distance);
		}
	}
	
	/*Returns the feature point distances of all surrounding quads*/
	@Hardcoded("Checking all surrounding 24 quads")
	private ArrayList<Double> featurePointDistances(final double x, final double y) {
		final int quadX = (int)Math.floor(x);
		final int quadY = (int)Math.floor(y);
		final ArrayList<Double> pointList = new ArrayList<Double>();
		
		/*I know, this is super slow. Performance was not my number one goal. I tried to reduce the amount of possible errors.*/
		for (int xx = quadX - 2; xx <= quadX + 2; xx++) {
			for (int yy = quadY - 2; yy <= quadY + 2; yy++) {
				featurePointDistances(xx, yy, x, y, pointList);
			}
		}
		
		return pointList;
	}
	
	/**
	 * Returns the noise value for the specified location.
	 * You can specify the functions used to calculate the value.
	 * See class description for details.
	 * @return A positive value which depends on the noise function parameters
	 */
	@Override
	public double getValue(final double x, final double y) {
		final ArrayList<Double> featurePointDistances = featurePointDistances(x, y);
		final int size = Math.max(density, MIN_FEATURE_POINTS_USED);
		final List<Double> distancesToBeUsed =  featurePointDistances.subList(0, size);
		final double value = valueFunction.apply(distancesToBeUsed);
		
		return value;
	}
	
	/**
	 * Sets the feature point density.
	 * @param density The density
	 */
	public void setDensity(final int density) {
		this.density = density;
	}
	
	/**
	 * Allows you to set the function which will be used to calculate the distance from the sample point to the feature points.
	 * The default is the distance squared.
	 * @param distanceFunction The distance function
	 */
	public void setDistanceFunction(final BiFunction<Vector2, Vector2, Double> distanceFunction) {
		this.distanceFunction = distanceFunction;
	}
	
	/**
	 * Sets the value function which is used to calculate the final noise value.
	 * The function parameter is a list which contains the ordered distances to the nearest <code>n</code> feature points where <code>n</code> is at least the density.
	 * So if you want to use the distance to the second nearest feature point as the noise value, just pass something like this: <pre>list -&gt; list.get(1)</pre>
	 * @param valueFunction The noise value function
	 */
	public void setValueFunction(final Function<List<Double>, Double> valueFunction) {
		this.valueFunction = valueFunction;
	}
	
	/**
	 * Returns the feature point density.
	 * @return The density
	 */
	public int getDensity() {
		return density;
	}
	
	/*Why is there no method in "java.lang.Maths" for this...?*/
	private static long factorial(int x) {
		if (x < 0) {
			throw new IllegalArgumentException(MESSAGE_NEGATIVE_PARAMETER);
		}
		
		long value = 1;
		
		while (x > 0) {
			value *= x;
			x--;
		}
		
		return value;
	}
	
	private static double poisson(final double lambda, final int x) {
		return Math.pow(lambda, x) / factorial(x) * Math.pow(Math.E, -lambda);
	}
	
	/*Assumes that the list is already sorted*/
	private static <T extends Comparable<T>> void insertSorted(final ArrayList<T> ts, final T t) {
		final int size = ts.size();
		
		if (size == 0) {
			ts.add(t);
			
			return;
		}
		
		for (int i = 0; i < size; i++) {
			final T entry = ts.get(i);
			final int result = t.compareTo(entry);
			
			if (result < 0) {
				ts.add(i, t);
				
				return;
			}
		}
		
		ts.add(t);
	}
	
}
