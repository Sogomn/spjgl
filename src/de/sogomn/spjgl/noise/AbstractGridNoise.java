/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.noise;

import java.util.Random;

abstract class AbstractGridNoise implements INoise2D {
	
	private int permutationMask;
	
	/**
	 * Permutation array to build hashes.
	 */
	protected int[] permutationX, permutationY;
	
	private static final int DEFAULT_PERMUTATION_BIT_DEPTH = 8;
	
	public AbstractGridNoise(final int permutationLengthBitDepth, final long seedX, final long seedY) {
		permute(permutationLengthBitDepth, seedX, seedY);
	}
	
	public AbstractGridNoise(final long seedX, final long seedY) {
		permute(DEFAULT_PERMUTATION_BIT_DEPTH, seedX, seedY);
	}
	
	/**
	 * Creates a hash value based on the specified coordinates and the permutations.
	 * @param x The x coordinate
	 * @param y The y coordinate
	 * @return The hash
	 */
	protected final int hash(int x, int y) {
		x &= permutationMask;
		y &= permutationMask;
		
		final int hash = permutationX[x ^ permutationY[y]];
		
		return hash;
	}
	
	/**
	 * Recreates the permutations based on the seeds.
	 * They are used to hash locations.
	 * The permutation lengths are equal to <code>1 &lt;&lt; bit depth</code>.
	 * Keep in mind that the permutations need <code>(1 &lt;&lt; bit depth) * 4 * 2</code> bytes of heap space.
	 * @param permutationLengthBitDepth The bit depth for the permutation length
	 * @param seedX The seed for the x-axis permutation
	 * @param seedY The seed for the y-axis permutation
	 */
	public final void permute(final int permutationLengthBitDepth, final long seedX, final long seedY) {
		final int permutationLength = 1 << permutationLengthBitDepth;
		
		permutationMask = permutationLength - 1;
		permutationX = createPermutation(permutationLength, seedX);
		permutationY = createPermutation(permutationLength, seedY);
	}
	
	/**
	 * Creates an array of random values.
	 * Yeah, that's not the definition of a permutation.
	 * I couldn't find a better name.
	 * @param length The permutation length
	 * @param seed The seed
	 * @return The permutation
	 */
	protected static int[] createPermutation(final int length, final long seed) {
		final int[] permutation = new int[length];
		final Random ran = new Random(seed);
		
		for (int i = 0; i < length; i++) {
			final int value = ran.nextInt();
			
			permutation[i] = value;
		}
		
		return permutation;
	}
	
}
