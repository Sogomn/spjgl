/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.noise;

import java.util.Random;

/**
 * Generates completely random values.
 * The output depends on the input. It's still noise.
 * Generates values from 0 to 1.
 * @author Sogomn
 *
 */
public final class RandomNoise extends AbstractGridNoise {
	
	private static final int DEFAULT_PERMUTATION_LENGTH_BIT_DEPTH = 8;
	
	/**
	 * Creates a new RandomNoise object.
	 * @see #permute(int, long, long)
	 * @param permutationLengthBitDepth The bit depth for the permutation length
	 * @param seedX The seed for the x-axis permutation
	 * @param seedY The seed for the y-axis permutation
	 */
	public RandomNoise(final int permutationLengthBitDepth, final long seedX, final long seedY) {
		super(permutationLengthBitDepth, seedX, seedY);
	}
	
	/**
	 * Constructs a new RandomNoise object with the default permutation length of 2<sup>8</sup>.
	 * @param seedX The seed for the x-axis permutation
	 * @param seedY The seed for the y-axis permutation
	 */
	public RandomNoise(final long seedX, final long seedY) {
		this(DEFAULT_PERMUTATION_LENGTH_BIT_DEPTH, seedX, seedY);
	}
	
	/**
	 * Constructs a new RandomNoise object with the default permutation length of 2<sup>8</sup> and random seeds.
	 */
	public RandomNoise() {
		this(randomSeed(), randomSeed());
	}
	
	/**
	 * Returns the noise value at the given location.
	 * @return A random value from 0 (inclusive) to 1 (exclusive)
	 */
	@Override
	public double getValue(final double x, final double y) {
		final int quadX = (int)Math.floor(x);
		final int quadY = (int)Math.floor(y);
		final int hash = hash(quadX, quadY);
		final long seed = Integer.toUnsignedLong(hash);
		final Random ran = new Random(seed);
		final double value = ran.nextDouble();
		
		return value;
	}
	
	private static long randomSeed() {
		final Random ran = new Random();
		
		return ran.nextLong();
	}
	
}
