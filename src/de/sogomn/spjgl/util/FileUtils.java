/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.util;

import java.net.URI;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Several useful methods related to files and IO-streams.
 * These are just convenience functions to make your game code less bloated.
 * There is no explicit exception handling; most methods return null on error.
 * @author Sogomn
 *
 */
public final class FileUtils {
	
	private FileUtils() {
		//...
	}
	
	/**
	 * Reads all data from the given file (classpath!).
	 * @param path The relative path to the file
	 * @return The data or null in case of failure
	 */
	public static byte[] readInternalBytes(final String path) {
		try {
			final URI uri = ClassLoader.getSystemResource(path).toURI();
			final Path p = Paths.get(uri);
			
			return Files.readAllBytes(p);
		} catch (final Exception ex) {
			ex.printStackTrace();
			
			return null;
		}
	}
	
	/**
	 * Reads all data from the given external file.
	 * @param path The path to the file - can be both relative and absolute
	 * @return The data or null in case of failure
	 */
	public static byte[] readExternalBytes(final String path) {
		final Path p = Paths.get(path);
		
		try {
			return Files.readAllBytes(p);
		} catch (final Exception ex) {
			ex.printStackTrace();
			
			return null;
		}
	}
	
	/**
	 * Reads all lines from the given file (classpath!).
	 * @param path The relative path to the file
	 * @return The lines or null in case of failure
	 */
	public static List<String> readInternalLines(final String path) {
		try {
			final URI uri = ClassLoader.getSystemResource(path).toURI();
			final Path p = Paths.get(uri);
			
			return Files.readAllLines(p);
		} catch (final Exception ex) {
			ex.printStackTrace();
			
			return null;
		}
	}
	
	/**
	 * Reads all lines from the given external file.
	 * @param path The path to the file - can be both relative and absolute
	 * @return The lines or null in case of failure
	 */
	public static List<String> readExternalLines(final String path) {
		final Path p = Paths.get(path);
		
		try {
			return Files.readAllLines(p);
		} catch (final Exception ex) {
			ex.printStackTrace();
			
			return null;
		}
	}
	
	/**
	 * Writes data to the given path.
	 * Creates a new file if it does not exist.
	 * @param path The absolute or relative path
	 * @param data The data to be written
	 * @return True on success; false otherwise
	 */
	public static boolean writeBytes(final String path, final byte[] data) {
		final Path p = Paths.get(path);
		
		try {
			Files.write(p, data);
			
			return true;
		} catch (final Exception ex) {
			ex.printStackTrace();
			
			return false;
		}
	}
	
	/**
	 * Writes lines of text to the given path.
	 * Appends a line break to every string.
	 * @param path The path
	 * @param lines The lines to be written
	 * @return True on success; false otherwise
	 */
	public static boolean writeLines(final String path, final Iterable<? extends CharSequence> lines) {
		final Path p = Paths.get(path);
		
		try {
			Files.write(p, lines);
			
			return true;
		} catch (final Exception ex) {
			ex.printStackTrace();
			
			return false;
		}
	}
	
	/**
	 * Creates a new file at the given path.
	 * Does nothing if the file already exists.
	 * @param path The path to the file to be created
	 * @return True on success (or if the file already existed); false otherwise
	 */
	public static boolean createFile(final String path) {
		final Path p = Paths.get(path);
		
		try {
			Files.createFile(p);
			
			return true;
		} catch (final FileAlreadyExistsException ex) {
			return true;
		} catch (final Exception ex) {
			ex.printStackTrace();
			
			return false;
		}
	}
	
	/**
	 * Deletes the specified file.
	 * Does nothing if the file doesn't exist.
	 * @param path The path to the file
	 * @return True if the file was deleted; false otherwise
	 */
	public static boolean deleteFile(final String path) {
		final Path p = Paths.get(path);
		
		try {
			return Files.deleteIfExists(p);
		} catch (final Exception ex) {
			ex.printStackTrace();
			
			return false;
		}
	}
	
	/**
	 * Creates a new folder and all necessary parent folders.
	 * Does nothing if the last directory already exists.
	 * @param path The path to the directory to be created
	 * @return True if the directories were created (or already existed); false otherwise
	 */
	public static boolean createDirectories(final String path) {
		final Path p = Paths.get(path);
		
		try {
			Files.createDirectories(p);
			
			return true;
		} catch (final FileAlreadyExistsException ex) {
			return true;
		} catch (final Exception ex) {
			ex.printStackTrace();
			
			return false;
		}
	}
	
}
