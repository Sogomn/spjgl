/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.util;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Supplier;

import de.sogomn.spjgl.IUpdatable;

/**
 * Schedule tasks synchronously with the update chain.
 * The method {@link #update(long)} needs to be called regularly in order to work.
 * Does not use a separate thread.
 * @author Sogomn
 *
 */
public final class Scheduler implements IUpdatable {
	
	private CopyOnWriteArrayList<Task> tasks;
	
	/**
	 * Constructs a new Scheduler which can execute tasks.
	 */
	public Scheduler() {
		tasks = new CopyOnWriteArrayList<Task>();
	}
	
	/**
	 * Updates the scheduler.
	 */
	@Override
	public void update(final long delta) {
		tasks.forEach(task -> {
			task.update(delta);
			
			if (task.isDone()) {
				task.execute();
			}
		});
		
		tasks.removeIf(Task::isDone);
	}
	
	/**
	 * Removes all tasks from the schedule without executing them.
	 */
	public void removeAllTasks() {
		tasks.clear();
	}
	
	/**
	 * Adds a task to the schedule.
	 * @param task The task
	 */
	public void addTask(final Task task) {
		tasks.add(task);
	}
	
	/**
	 * Removes a task from the schedule.
	 * @param task The task to be removed
	 */
	public void removeTask(final Task task) {
		tasks.remove(task);
	}
	
	/**
	 * Returns the amount of tasks currently scheduled.
	 * @return The task count
	 */
	public int taskCount() {
		return tasks.size();
	}
	
	/**
	 * Returns whether the scheduler has a task scheduled or not.
	 * @return True if there is a task scheduled; false otherwise.
	 */
	public boolean hasTask() {
		return !tasks.isEmpty();
	}
	
	/**
	 * Can be scheduled with the help of the Scheduler class
	 * @author Sogomn
	 *
	 */
	public static final class Task implements IUpdatable {
		
		private final Supplier<Boolean> task;
		
		private long timer;
		private final long delay;
		
		/**
		 * Constructs a new Task object.
		 * If the specified supplier returns true the task will reschedule itself after execution.
		 * @param delay The time the task should be executed after in nanoseconds
		 * @param task The supplier
		 */
		public Task(final long delay, final Supplier<Boolean> task) {
			this.delay = delay;
			this.task = task;
		}
		
		/**
		 * Updates the task.
		 * Gets called by the Scheduler automatically.
		 */
		@Override
		public void update(final long delta) {
			timer += delta;
		}
		
		/**
		 * Resets the internal timer of the task.
		 */
		public void reset() {
			timer = 0;
		}
		
		/**
		 * Returns the time the task should be executed after in seconds.
		 * Does not change, even when updated.
		 * @return The time
		 */
		public long getDelay() {
			return delay;
		}
		
		/**
		 * Returns whether this Task is done or not.
		 * @return The state
		 */
		public boolean isDone() {
			return timer >= delay;
		}
		
		/**
		 * Executes this task. Ignores the timer.
		 * Reschedules the task if the specified supplier returns true.
		 */
		public void execute() {
			final boolean reschedule = task.get();
			
			if (reschedule) {
				reset();
			}
		}
		
	}
	
}
