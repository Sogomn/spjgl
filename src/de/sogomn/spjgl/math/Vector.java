/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.math;

import java.util.Arrays;
import java.util.function.BiFunction;

import de.sogomn.spjgl.debug.Hardcoded;

/**
 * Vector class.
 * All methods which perform a calculation with a second vector return a copy instead of the original object and do not modify the operands.
 * @author Sogomn
 *
 */
public strictfp class Vector {
	
	/**
	 * The elements of the vector.
	 */
	protected final double[] elements;
	
	private static final String MESSAGE_ELEMENT_COUNT = "Invalid element count";
	
	/**
	 * Constructs a new vector with the specified amount of elements.
	 * @param elementCount The number of elements in the vector
	 */
	public Vector(final int elementCount) {
		elements = new double[elementCount];
	}
	
	/**
	 * Constructs a new vector which contains the specified elements.
	 * The element count is equal to the length of the passed array.
	 * @param elements The vector elements
	 */
	public Vector(final double... elements) {
		this.elements = elements;
	}
	
	/**
	 * Performs the specified operation with the vector and the one specified and returns the result as a new vector.
	 * The BiFunction parameters are the current vector element values.
	 * @param other The other operand
	 * @param operation The operation
	 * @throws IllegalArgumentException If the amount of elements in the vectors is not equal
	 * @return A new vector - The result
	 */
	protected final Vector performOperation(final Vector other, final BiFunction<Double, Double, Double> operation) {
		if (elements.length != other.elements.length) {
			throw new IllegalArgumentException(MESSAGE_ELEMENT_COUNT);
		}
		
		final Vector result = new Vector(elements.length);
		
		for (int i = 0; i < result.elements.length; i++) {
			result.elements[i] = operation.apply(elements[i], other.elements[i]);
		}
		
		return result;
	}
	
	/**
	 * Generates the hash code based on the elements.
	 */
	@Hardcoded
	@Override
	public int hashCode() {
		final int hash = 31 + Arrays.hashCode(elements);
		
		return hash;
	}
	
	/**
	 * Compares the elements of the vectors.
	 */
	@Override
	public boolean equals(final Object object) {
		final Class<?> clazz = object.getClass();
		
		if (object == null || !clazz.isInstance(object)) {
			return false;
		} else if (this == object) {
			return true;
		}
		
		final Vector other = (Vector)object;
		
		return Arrays.equals(elements, other.elements);
	}
	
	/**
	 * Returns a string representation of the vector.
	 */
	@Hardcoded
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		
		builder.append("[ ");
		
		for (int i = 0; i < elements.length; i++) {
			final double value = elements[i];
			final String formatted = String.format("%.2f ", value);
			
			builder.append(formatted);
		}
		
		builder.append("]");
		
		return builder.toString();
	}
	
	/**
	 * Calculates the distance vector to another one.
	 * Calls {@link #performOperation(Vector, BiFunction)}.
	 * @param other The addend
	 * @return The sum of the two vectors
	 */
	public Vector add(final Vector other) {
		final BiFunction<Double, Double, Double> sum = (a, b) -> a + b;
		
		return performOperation(other, sum);
	}
	
	/**
	 * Subtracts another vector from the vector.
	 * Calls {@link #performOperation(Vector, BiFunction)}.
	 * @param other The subtrahend
	 * @return The difference between the two vectors
	 */
	public Vector subtract(final Vector other) {
		final BiFunction<Double, Double, Double> difference = (a, b) -> a - b;
		
		return performOperation(other, difference);
	}
	
	/**
	 * Multiplies another vector with the vector.
	 * Calls {@link #performOperation(Vector, BiFunction)}.
	 * @param other The multiplier
	 * @return The product of the two vectors
	 */
	public Vector multiply(final Vector other) {
		final BiFunction<Double, Double, Double> product = (a, b) -> a * b;
		
		return performOperation(other, product);
	}
	
	/**
	 * Divides the vector by another vector.
	 * Calls {@link #performOperation(Vector, BiFunction)}.
	 * @param other The divisor
	 * @return The quotient of the two vectors
	 */
	public Vector divide(final Vector other) {
		final BiFunction<Double, Double, Double> quotient = (a, b) -> a / b;
		
		return performOperation(other, quotient);
	}
	
	/**
	 * Calculates the dot product of the passed vector and the calling one.
	 * Calls {@link #multiply(Vector)}.
	 * @param other The other vector
	 * @return The dot product
	 */
	public double dot(final Vector other) {
		final Vector product = multiply(other);
		
		double result = 0;
		
		for (int i = 0; i < product.elements.length; i++) {
			result += product.elements[i];
		}
		
		return result;
	}
	
	/**
	 * Returns a normalized copy of the vector.
	 * @return The normalized copy of the vector
	 */
	public Vector normalize() {
		final double length = length();
		final Vector result = new Vector(elements.length);
		
		for (int i = 0; i < result.elements.length; i++) {
			result.elements[i] = elements[i] / length;
		}
		
		return result;
	}
	
	/**
	 * Returns the squared length of the vector.
	 * Much faster than regular length calculation.
	 * @return The squared length
	 */
	public double lengthSquared() {
		double result = 0;
		
		for (int i = 0; i < elements.length; i++) {
			final double value = elements[i];
			
			result += value * value;
		}
		
		return result;
	}
	
	/**
	 * Calculates the length of the vector.
	 * @return The length
	 */
	public double length() {
		final double lengthSquared = lengthSquared();
		final double result = Math.sqrt(lengthSquared);
		
		return result;
	}
	
	/**
	 * Sets the value at the given index.
	 * No explicit bounds checking.
	 * @param index The element index
	 * @param value The value
	 */
	public void set(final int index, final double value) {
		elements[index] = value;
	}
	
	/**
	 * Returns the value at the given index.
	 * No explicit bounds checking.
	 * @param index The index
	 * @return The value
	 */
	public double get(final int index) {
		return elements[index];
	}
	
	/**
	 * Returns the amount of elements in the vector.
	 * @return The element count
	 */
	public final int elements() {
		return elements.length;
	}
	
}
