/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.math;

import de.sogomn.spjgl.debug.Hardcoded;

/**
 * Vector class with 2 elements.
 * Just to make working with 2D vectors easier.
 * @author Sogomn
 *
 */
@Hardcoded("X = 0, Y = 1. Element count always assumed to be 2.")
public final strictfp class Vector2 extends Vector {
	
	/**
	 * Creates a new Vector2 object with the specified x and y coordinates.
	 * @param x The x coordinate
	 * @param y The y coordiante
	 */
	public Vector2(final double x, final double y) {
		super(x, y);
	}
	
	/**
	 * Creates a new Vector2 object with x and y being 0.
	 */
	public Vector2() {
		this(0, 0);
	}
	
	@Override
	public Vector2 normalize() {
		final double length = length();
		final double newX = elements[0] / length;
		final double newY = elements[1] / length;
		final Vector2 result = new Vector2(newX, newY);
		
		return result;
	}
	
	/**
	 * Adds two vectors.
	 * @param other The other vector
	 * @return The sum
	 * @see Vector#add(Vector)
	 */
	public Vector2 add(final Vector2 other) {
		final double newX = elements[0] + other.elements[0];
		final double newY = elements[1] + other.elements[1];
		final Vector2 result = new Vector2(newX, newY);
		
		return result;
	}
	
	/**
	 * Subtracts another one from the vector.
	 * @param other The other vector
	 * @return The difference
	 * @see Vector#subtract(Vector)
	 */
	public Vector2 subtract(final Vector2 other) {
		final double newX = elements[0] - other.elements[0];
		final double newY = elements[1] - other.elements[1];
		final Vector2 result = new Vector2(newX, newY);
		
		return result;
	}
	
	/**
	 * Multiplies two vectors.
	 * @param other The other vector
	 * @return The product
	 * @see Vector#multiply(Vector)
	 */
	public Vector2 multiply(final Vector2 other) {
		final double newX = elements[0] * other.elements[0];
		final double newY = elements[1] * other.elements[1];
		final Vector2 result = new Vector2(newX, newY);
		
		return result;
	}
	
	/**
	 * Divides the vector by another one.
	 * @param other The other vector
	 * @return The quotient
	 * @see Vector#divide(Vector)
	 */
	public Vector2 divide(final Vector2 other) {
		final double newX = elements[0] / other.elements[0];
		final double newY = elements[1] / other.elements[1];
		final Vector2 result = new Vector2(newX, newY);
		
		return result;
	}
	
	/**
	 * Returns a rotated copy of the vector.
	 * Performs the rotation around (0 | 0).
	 * @param degrees The angle of the rotation in degrees
	 * @return A roated copy of the vector
	 */
	public Vector2 rotate(final double degrees) {
		final double radians = Math.toRadians(degrees);
		final double x = elements[0];
		final double y = elements[1];
		final double newX = x * Math.cos(radians) - y * Math.sin(radians);
		final double newY = y * Math.cos(radians) + x * Math.sin(radians);
		final Vector2 result = new Vector2(newX, newY);
		
		return result;
	}
	
	/**
	 * Sets the x and y coordinates of the vector.
	 * Be careful that the first element is not an int, otherwise you call {@link Vector#set(int, double)}.
	 * @param x The new x
	 * @param y The new y
	 */
	public void set(final double x, final double y) {
		elements[0] = x;
		elements[1] = y;
	}
	
	/**
	 * Sets the x coordinate of the vector.
	 * @param x The new x
	 */
	public void setX(final double x) {
		elements[0] = x;
	}
	
	/**
	 * Sets the y coordinate of the vector.
	 * @param y The new y
	 */
	public void setY(final double y) {
		elements[1] = y;
	}
	
	/**
	 * Returns the x coordinate.
	 * @return The x coordinate
	 */
	public double getX() {
		return elements[0];
	}
	
	/**
	 * Returns the y coordinate.
	 * @return The y coordinate
	 */
	public double getY() {
		return elements[1];
	}
	
	/**
	 * Constructs a new Vector2 object whose elements are equal to the first two elements of the specified one.
	 * <pre>
	 * (9, 2) -&gt; (9, 2)
	 * (3, 7, 8) -&gt; (3, 7)
	 * (5) -&gt; (5, 0)
	 * </pre>
	 * @param vector The vector to "copy"
	 * @return The vector
	 */
	public static Vector2 from(final Vector vector) {
		final Vector2 copy = new Vector2();
		final int elements = Math.min(copy.elements(), vector.elements());
		
		for (int i = 0; i < elements; i++) {
			copy.elements[i] = vector.elements[i];
		}
		
		return copy;
	}
	
}
