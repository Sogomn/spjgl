/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.math;

import java.util.Arrays;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

import de.sogomn.spjgl.debug.Hardcoded;

/**
 * Matrix class.
 * All methods which perform a calculation with a second matrix return a copy instead of the original object and do not modify the operands.
 * @author Sogomn
 *
 */
public strictfp class Matrix {
	
	/**
	 * The amount of rows and columns in the matrix.
	 */
	protected final int rows, columns;
	
	/**
	 * The elements of the matrix.
	 */
	protected final double[] elements;
	
	private static final String MESSAGE_ROW_COLUMN_COUNT = "Invalid row / column count";
	
	/**
	 * Creates a new matrix object with the specified amount rows and columns.
	 * @param rows The rows
	 * @param columns The columns
	 */
	public Matrix(final int rows, final int columns) {
		this.rows = rows;
		this.columns = columns;
		
		elements = new double[rows * columns];
	}
	
	/**
	 * Applies the specified BiConsumer to all elements in the matrix.
	 * The BiConsumer parameters are the current row and column.
	 * @param operation The operation to perform
	 */
	protected final void forEachElement(final BiConsumer<Integer, Integer> operation) {
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < columns; c++) {
				operation.accept(r, c);
			}
		}
	}
	
	/**
	 * Performs the specified operation with another matrix.
	 * Does not modify the operands.
	 * The BiFunction parameters are the current matrix values.
	 * @param other The second operand
	 * @param operation The operation
	 * @throws IllegalArgumentException If the amount of columns is not equal to the amount of rows of the argument
	 * @return A new matrix - The result
	 */
	protected final Matrix performOperation(final Matrix other, final BiFunction<Double, Double, Double> operation) {
		if (rows != other.rows || columns != other.columns) {
			throw new IllegalArgumentException(MESSAGE_ROW_COLUMN_COUNT);
		}
		
		final Matrix result = new Matrix(rows, columns);
		
		result.forEachElement((r, c) -> {
			final int index = r * columns + c;
			
			result.elements[index] = operation.apply(elements[index], other.elements[index]);
		});
		
		return result;
	}
	
	/**
	 * Generates the hash code based on the elements.
	 */
	@Hardcoded
	@Override
	public int hashCode() {
		final int hash = 31 + Arrays.hashCode(elements);
		
		return hash;
	}
	
	/**
	 * Compares the elements of the matrices.
	 */
	@Override
	public boolean equals(final Object object) {
		final Class<?> clazz = object.getClass();
		
		if (object == null || !clazz.isInstance(object)) {
			return false;
		} else if (this == object) {
			return true;
		}
		
		final Matrix other = (Matrix)object;
		
		return Arrays.equals(elements, other.elements);
	}
	
	/**
	 * Returns a string representation of the matrix.
	 */
	@Hardcoded
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		
		for (int r = 0; r < rows; r++) {
			builder.append("[ ");
			
			for (int c = 0; c < columns; c++) {
				final double value = get(r, c);
				final String formatted = String.format("%.2f ", value);
				
				builder.append(formatted);
			}
			
			builder.append("]\r\n");
		}
		
		return builder.toString();
	}
	
	/**
	 * Sums the elements of the specified row together and returns the result.
	 * @param row The row
	 * @return The elements of the row summed up
	 */
	public double sumRow(final int row) {
		if (columns == 1) {
			return elements[row];
		}
		
		final int indexStart = row * columns;
		final int indexEnd = indexStart + columns - 1;
		
		double sum = 0;
		
		for (int i = indexStart; i <= indexEnd; i++) {
			sum += elements[i];
		}
		
		return sum;
	}
	
	/**
	 * Sums the elements of the specified column together and returns the result.
	 * @param column The column
	 * @return The elements of the column summed up
	 */
	public double sumColumn(final int column) {
		if (rows == 1) {
			return elements[column];
		}
		
		final int indexEnd = (rows - 1) * columns + column;
		
		double sum = 0;
		
		for (int i = column; i <= indexEnd; i += columns) {
			sum += elements[i];
		}
		
		return sum;
	}
	
	/**
	 * Performs a matrix addition.
	 * The calling matrix is the left and the argument is the right operand.
	 * Calls {@link #performOperation(Matrix, BiFunction)}.
	 * @param other The matrix to perform the addition with
	 * @return The resulting matrix
	 */
	public Matrix add(final Matrix other) {
		final BiFunction<Double, Double, Double> sum = (a, b) -> a + b;
		
		return performOperation(other, sum);
	}
	
	/**
	 * Performs a matrix subtraction.
	 * The calling matrix is the left and the argument is the right operand.
	 * Calls {@link #performOperation(Matrix, BiFunction)}.
	 * @param other The matrix to perform the subtraction with
	 * @return The resulting matrix
	 */
	public Matrix subtract(final Matrix other) {
		final BiFunction<Double, Double, Double> difference = (a, b) -> a - b;
		
		return performOperation(other, difference);
	}
	
	/**
	 * Performs a matrix multiplication.
	 * The calling matrix is the left and the argument is the right operand.
	 * @param other The matrix to multiply with
	 * @throws IllegalArgumentException If the amount of columns is not equal to the amount of rows of the argument
	 * @return The resulting matrix
	 */
	public Matrix multiply(final Matrix other) {
		if (columns != other.rows) {
			throw new IllegalArgumentException(MESSAGE_ROW_COLUMN_COUNT);
		}
		
		final Matrix result = new Matrix(rows, other.columns);
		
		result.forEachElement((r, c) -> {
			for (int j = 0; j < columns && j < other.rows; j++) {
				final double value = get(r, j) * other.get(j, c);
				final int index = r * result.columns + c;
				
				result.elements[index] += value;
			}
		});
		
		return result;
	}
	
	/**
	 * Sets a matrix element to the specified value.
	 * No explicit bounds checking.
	 * @param row The row
	 * @param column The column
	 * @param value The value
	 */
	public void set(final int row, final int column, final double value) {
		final int index = row * columns + column;
		
		elements[index] = value;
	}
	
	/**
	 * Returns the value of the specified matrix element.
	 * No explicit bounds checking.
	 * @param row The row
	 * @param column The column
	 * @return The value
	 */
	public double get(final int row, final int column) {
		final int index = row * columns + column;
		
		return elements[index];
	}
	
	/**
	 * Returns the amount of rows the matrix has.
	 * @return The rows
	 */
	public final int rows() {
		return rows;
	}
	
	/**
	 * Returns the amount of columns the matrix has.
	 * @return The columns
	 */
	public final int columns() {
		return columns;
	}
	
	/**
	 * Constructs a one-row matrix from the specified vector.
	 * @param vector The vector which contains the elements to be used
	 * @return The matrix
	 */
	public static Matrix rowMatrix(final Vector vector) {
		final int length = vector.elements();
		final Matrix matrix = new Matrix(1, length);
		
		for (int i = 0; i < length; i++) {
			final double value = vector.get(i);
			
			matrix.set(0, i, value);
		}
		
		return matrix;
	}
	
	/**
	 * Constructs a one-column matrix from the specified vector.
	 * @param vector The vector which contains the elements to be used
	 * @return The matrix
	 */
	public static Matrix columnMatrix(final Vector vector) {
		final int length = vector.elements();
		final Matrix matrix = new Matrix(length, 1);
		
		for (int i = 0; i < length; i++) {
			final double value = vector.get(i);
			
			matrix.set(i, 0, value);
		}
		
		return matrix;
	}
	
}
