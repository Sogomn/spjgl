/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.common;

/**
 * Holds the different resize behaviors for content inside a container.
 * @author Sogomn
 */
public enum ResizeBehavior {
	
	/**
	 * The content fills the container.
	 */
	STRETCH,
	
	/**
	 * The content fills as much as possible but maintains the aspect ratio.
	 */
	KEEP_ASPECT_RATIO,
	
	/**
	 * The content size does not change.
	 * It is displayed in the center of the container.
	 */
	KEEP_SIZE;
	
	ResizeBehavior() {
		;
	}
	
}
