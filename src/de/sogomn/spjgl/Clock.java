/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl;

import de.sogomn.spjgl.common.AbstractListenerContainer;

/**
 * A class to be used for continuous updating (e.g. for a game loop).
 * It is recommended to call the {@link #update()} method in a regular interval.
 * @author Sogomn
 *
 */
public final class Clock extends AbstractListenerContainer<IUpdatable> {
	
	private long initialTime, lastTime;
	private long ticks;
	
	private static final double NANOSECONDS_PER_SECOND = 1000000000.0;
	
	/**
	 * Constructs a new Clock object.
	 */
	public Clock() {
		reset();
	}
	
	/**
	 * Updates the clock and notifies all listeners.
	 * Passes them the elapsed time in nanoseconds.
	 * @return The elapsed time since the last update in nanoseconds
	 */
	public long update() {
		final long now = System.nanoTime();
		final long elapsed = System.nanoTime() - lastTime;
		
		notifyListeners(updatable -> updatable.update(elapsed));
		
		lastTime = now;
		ticks++;
		
		return elapsed;
	}
	
	/**
	 * Resets the tick counter and the starting time of the clock.
	 */
	public void reset() {
		initialTime = lastTime = System.nanoTime();
		ticks = 0;
	}
	
	/**
	 * Returns the tick counter.
	 * It increases by one every time {@link #update()} gets called.
	 * @return The tick counter
	 */
	public long tickCount() {
		return ticks;
	}
	
	/**
	 * Returns the elapsed time since the clock was started.
	 * @return The elapsed time in nanoseconds
	 */
	public long elapsed() {
		final long elapsed = System.nanoTime() - initialTime;
		
		return elapsed;
	}
	
	/**
	 * Returns the elapsed time since the last update.
	 * Neither increments the tick count nor notifies the listeners.
	 * The last update time stays the same aswell.
	 * @return The elapsed time in nanoseconds
	 */
	public long elapsedSinceLastUpdate() {
		final long elapsed = System.nanoTime() - lastTime;
		
		return elapsed;
	}
	
	/**
	 * Returns the time in nanoseconds to seconds.
	 * @param nanoseconds The nanoseconds
	 * @return The seconds as a double
	 */
	public static double asSeconds(final long nanoseconds) {
		return nanoseconds / NANOSECONDS_PER_SECOND;
	}
	
}
