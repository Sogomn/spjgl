/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui.wrapper;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.Painter;

import de.sogomn.spjgl.ui.AbstractComponent;
import de.sogomn.spjgl.ui.AbstractWrapper;

/**
 * Component wrapper to draw a background under the component.
 */
public final class Background extends AbstractWrapper {
	
	private Painter<AbstractComponent> background;
	
	/**
	 * Creates a new background wrapper with a transparent background.
	 * @param inner The inner component
	 */
	public Background(final AbstractComponent inner) {
		super(inner);
	}
	
	/**
	 * Creates a new background with a fixed color.
	 * @param inner The inner component
	 * @param color The color
	 */
	public Background(final AbstractComponent inner, final Color color) {
		super(inner);
		
		setBackground(color);
	}
	
	/**
	 * Creates a new background with the specified painter.
	 * The passed component is this wrapper, not the inner component.
	 * @param inner The inner component
	 * @param background The background painter
	 */
	public Background(final AbstractComponent inner, final Painter<AbstractComponent> background) {
		super(inner);
		
		this.background = background;
	}
	
	/**
	 * Creates a new background with a fixed image.
	 * @param inner The inner component
	 * @param image The image
	 */
	public Background(final AbstractComponent inner, final BufferedImage image) {
		super(inner);
		
		setBackground(image);
	}
	
	@Override
	public void draw(final Graphics2D g, final int width, final int height) {
		if (background != null) {
			background.paint(g, this, width, height);
		}
		
		super.draw(g, width, height);
	}
	
	/**
	 * Sets the background painter.
	 * The component passed is this wrapper, not the inner component.
	 * @param background The painter
	 */
	public void setBackground(final Painter<AbstractComponent> background) {
		this.background = background;
	}
	
	/**
	 * Sets the background to a fixed color.
	 * @param color The color
	 */
	public void setBackground(final Color color) {
		background = (g, c, w, h) -> {
			g.setColor(color);
			g.fillRect(0, 0, w, h);
		};
	}
	
	/**
	 * Sets the background to a fixed image.
	 * @param image The image
	 */
	public void setBackground(final BufferedImage image) {
		background = (g, c, w, h) -> g.drawImage(image, 0, 0, w, h, null);
	}
	
	/**
	 * Returns the current background.
	 * @return The background
	 */
	public Painter<AbstractComponent> getBackground() {
		return background;
	}
	
}
