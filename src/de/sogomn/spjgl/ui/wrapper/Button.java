/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui.wrapper;

import java.awt.event.MouseEvent;

import de.sogomn.spjgl.ui.AbstractComponent;
import de.sogomn.spjgl.ui.AbstractWrapper;

/**
 * A wrapper around a component to act as a button.
 * @author Sogomn
 */
public final class Button extends AbstractWrapper {
	
	private boolean pressed;
	private Runnable action;
	
	/**
	 * Creates a new button.
	 * @param inner The inner component
	 */
	public Button(final AbstractComponent inner) {
		super(inner);
	}
	
	@Override
	protected void focusChangeEvent(final boolean focused) {
		pressed = pressed && focused;
		
		super.focusChangeEvent(focused);
	}
	
	@Override
	public void mouseEvent(final int x, final int y, final int button, final boolean flag) {
		if (button == MouseEvent.BUTTON1) {
			if (!flag && pressed && action != null) {
				action.run();
			}
			
			pressed = flag;
		}
		
		super.mouseEvent(x, y, button, flag);
	}
	
	/**
	 * Sets the action to perform when the button has been clicked.
	 * @param action The action
	 */
	public void setAction(final Runnable action) {
		this.action = action;
	}
	
	/**
	 * Returns whether the button is being pressed.
	 * @return The state
	 */
	public boolean isPressed() {
		return pressed;
	}
	
	/**
	 * Returns the action to perform when the button has been pressed
	 * @return The action to perform
	 */
	public Runnable getAction() {
		return action;
	}
	
}
