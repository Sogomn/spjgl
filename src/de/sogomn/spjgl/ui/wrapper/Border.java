/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui.wrapper;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import de.sogomn.spjgl.ui.AbstractComponent;
import de.sogomn.spjgl.ui.AbstractWrapper;

/**
 * A wrapper to display a border around a component.
 * @author Sogomn
 */
public final class Border extends AbstractWrapper {
	
	private Color color;
	private int thickness;
	
	private static final int DEFAULT_THICKNESS = 1;
	
	/**
	 * Creates a new border.
	 * @param inner The inner component
	 * @param color The border color
	 * @param thickness The border thickness
	 */
	public Border(final AbstractComponent inner, final Color color, final int thickness) {
		super(inner);
		
		this.color = color;
		this.thickness = thickness;
	}
	
	/**
	 * Creates a new border whith a default thickness of 1.
	 * @param inner The inner component
	 * @param color The border color
	 */
	public Border(final AbstractComponent inner, final Color color) {
		this(inner, color, DEFAULT_THICKNESS);
	}
	
	@Override
	public void draw(final Graphics2D g, final int width, final int height) {
		super.draw(g, width, height);
		
		final Stroke oldStroke = g.getStroke();
		final BasicStroke stroke = new BasicStroke(thickness);
		
		g.setColor(color);
		g.setStroke(stroke);
		g.drawRect(0, 0, width - 1, height - 1);
		g.setStroke(oldStroke);
	}
	
	/**
	 * Sets the border color.
	 * @param color The new color
	 */
	public void setColor(final Color color) {
		this.color = color;
	}
	
	/**
	 * Sets the border thickness.
	 * @param thickness The new thickness
	 */
	public void setThickness(final int thickness) {
		this.thickness = thickness;
	}
	
	/**
	 * Returns the border color.
	 * @return The color
	 */
	public Color getColor() {
		return color;
	}
	
	/**
	 * Returns the border thickness.
	 * @return The thickness
	 */
	public int getThickness() {
		return thickness;
	}
	
}
