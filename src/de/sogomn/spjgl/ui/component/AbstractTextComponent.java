/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui.component;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

import de.sogomn.spjgl.ui.AbstractComponent;
import de.sogomn.spjgl.ui.IFontMetricsProvider;

abstract class AbstractTextComponent<T> extends AbstractComponent {
	
	protected T text;
	protected int padding;
	protected Color foreground;
	protected Font font;
	
	private static final int DEFAULT_PADDING = 5;
	private static final Color DEFAULT_FOREGROUND = Color.BLACK;
	private static final Font DEFAULT_FONT = new Font(Font.MONOSPACED, Font.PLAIN, 15);
	
	public AbstractTextComponent(final T text) {
		this.text = text;
		
		padding = DEFAULT_PADDING;
		foreground = DEFAULT_FOREGROUND;
		font = DEFAULT_FONT;
	}
	
	@Override
	protected int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height) {
		final FontMetrics fm = fmp.getFontMetrics(font);
		final String text = getText();
		
		return fm.stringWidth(text) + padding * 2;
	}
	
	@Override
	protected int calculatePreferredHeight(final IFontMetricsProvider fmp, final int width) {
		final FontMetrics fm = fmp.getFontMetrics(font);
		
		return fm.getHeight() + padding * 2;
	}
	
	protected abstract void drawText(final Graphics2D g, final int width, final int height);
	
	@Override
	public void draw(final Graphics2D g, final int width, final int height) {
		g.setColor(foreground);
		g.setFont(font);
		
		drawText(g, width, height);
	}
	
	/**
	 * Sets the component text.
	 * @param text The new text
	 */
	public abstract void setText(final String text);
	
	/**
	 * Sets the text padding.
	 * @param padding The padding
	 */
	public final void setPadding(final int padding) {
		this.padding = padding;
	}
	
	/**
	 * Sets the foregound color.
	 * @param foreground The color
	 */
	public final void setForeground(final Color foreground) {
		this.foreground = foreground;
	}
	
	/**
	 * Sets the font.
	 * @param font The font
	 */
	public final void setFont(final Font font) {
		this.font = font;
	}
	
	/**
	 * Returns the component text.
	 * @return The text
	 */
	public abstract String getText();
	
	/**
	 * Returns the text padding.
	 * @return The padding
	 */
	public final int getPadding() {
		return padding;
	}
	
	/**
	 * Returns the foreground color.
	 * @return The foreground color
	 */
	public final Color getForeground() {
		return foreground;
	}
	
	/**
	 * Returns the font.
	 * @return The font
	 */
	public final Font getFont() {
		return font;
	}
	
}
