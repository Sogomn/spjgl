package de.sogomn.spjgl.ui.component;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import de.sogomn.spjgl.common.ResizeBehavior;
import de.sogomn.spjgl.ui.AbstractComponent;
import de.sogomn.spjgl.ui.IFontMetricsProvider;

/**
 * Display images as UI elements.
 * The image can be null.
 * @author Sogomn
 */
public final class Frame extends AbstractComponent {
	
	private BufferedImage image;
	
	private ResizeBehavior resizeBehavior;
	
	/**
	 * Creates a new frame.
	 * @param image The image to display
	 * @param resizeBehavior The resize behavior of the image
	 */
	public Frame(final BufferedImage image, final ResizeBehavior resizeBehavior) {
		this.image = image;
		this.resizeBehavior = resizeBehavior;
	}
	
	/**
	 * Creates a new frame.
	 * The default resize behavior maintains the aspect ratio.
	 * @param image The image to display
	 */
	public Frame(final BufferedImage image) {
		this(image, ResizeBehavior.KEEP_ASPECT_RATIO);
	}
	
	private void keepAspectRatio(final Graphics2D g, final int width, final int height) {
		if (image == null) {
			return;
		}
		
		final int imageWidth = image.getWidth();
		final int imageHeight = image.getHeight();
		final double ratio = (double)width / height;
		final double imageRatio = (double)imageWidth / imageHeight;
		
		int renderWidth, renderHeight;
		
		if (ratio > imageRatio) {
			renderHeight = height;
			renderWidth = (int)(height * imageRatio);
		} else {
			renderWidth = width;
			renderHeight = (int)(width / imageRatio);
		}
		
		final int renderX = width / 2 - renderWidth / 2;
		final int renderY = height / 2 - renderHeight / 2;
		
		g.drawImage(image, renderX, renderY, renderWidth, renderHeight, null);
	}
	
	private void stretch(final Graphics2D g, final int width, final int height) {
		g.drawImage(image, 0, 0, width, height, null);
	}
	
	private void keepSize(final Graphics2D g, final int width, final int height) {
		if (image == null) {
			return;
		}
		
		final int imageWidth = image.getWidth();
		final int imageHeight = image.getHeight();
		final int renderX = width / 2 - imageWidth / 2;
		final int renderY = height / 2 - imageHeight / 2;
		
		g.drawImage(image, renderX, renderY, null);
	}
	
	@Override
	protected int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height) {
		if (image != null) {
			return image.getWidth();
		} else {
			return 0;
		}
	}
	
	@Override
	protected int calculatePreferredHeight(final IFontMetricsProvider fmp, final int width) {
		if (image != null) {
			return image.getHeight();
		} else {
			return 0;
		}
	}
	
	@Override
	protected void focusChangeEvent(final boolean focused) {
		;
	}
	
	@Override
	protected void hoverChangeEvent(final boolean hovered) {
		;
	}
	
	@Override
	public void draw(final Graphics2D g, final int width, final int height) {
		if (resizeBehavior == ResizeBehavior.KEEP_ASPECT_RATIO) {
			keepAspectRatio(g, width, height);
		} else if (resizeBehavior == ResizeBehavior.STRETCH) {
			stretch(g, width, height);
		} else if (resizeBehavior == ResizeBehavior.KEEP_SIZE) {
			keepSize(g, width, height);
		}
	}
	
	@Override
	public void mouseEvent(final int x, final int y, final int button, final boolean flag) {
		;
	}
	
	@Override
	public void mouseMotionEvent(final int x, final int y, final int modifiers) {
		;
	}
	
	@Override
	public void mouseWheelEvent(final int x, final int y, final int rotation) {
		;
	}
	
	@Override
	public void keyboardEvent(final int key, final char c, final int modifiers, final boolean flag) {
		;
	}
	
	/**
	 * Sets the image.
	 * @param image The new image
	 */
	public void setImage(final BufferedImage image) {
		this.image = image;
	}
	
	public void setResizeBehavior(final ResizeBehavior resizeBehavior) {
		this.resizeBehavior = resizeBehavior;
	}
	
	/**
	 * Returns the image.
	 * @return The current image
	 */
	public BufferedImage getImage() {
		return image;
	}
	
	public ResizeBehavior getResizeBehavior() {
		return resizeBehavior;
	}
	
}
