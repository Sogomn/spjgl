/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui.component;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.function.BiConsumer;

/**
 * A single-line text field.
 * Supports the most common editing operations.
 * @author Sogomn
 */
public final class TextField extends AbstractTextComponent<StringBuilder> {
	
	private int caretPosition;
	private int selectionStart, selectionLength;
	
	private Color selectionColor;
	
	private Runnable action;
	
	private static final Color DEFAULT_SELECTION_COLOR = new Color(0, 0, 0, 125);
	
	/**
	 * Creates a new text field.
	 */
	public TextField() {
		super(new StringBuilder());
		
		selectionColor = DEFAULT_SELECTION_COLOR;
	}
	
	private void withSelectionLeftRight(final BiConsumer<Integer, Integer> consumer) {
		final int selectionLeft, selectionRight;
		
		if (selectionLength > 0) {
			selectionLeft = selectionStart;
			selectionRight = selectionStart + selectionLength;
		} else if (selectionLength < 0) {
			selectionLeft = selectionStart + selectionLength;
			selectionRight = selectionStart;
		} else {
			selectionLeft = selectionRight = 0;
		}
		
		consumer.accept(selectionLeft, selectionRight);
	}
	
	private void pasteClipboard() {
		try {
			final String clipboard = (String)Toolkit.getDefaultToolkit()
				.getSystemClipboard()
				.getData(DataFlavor.stringFlavor);
			
			if (clipboard != null) {
				insert(clipboard);
			}
		} catch (final IOException | UnsupportedFlavorException ex) {
			ex.printStackTrace();
		}
	}
	
	private void copyToClipboard() {
		withSelectionLeftRight((left, right) -> {
			final String selection = text.substring(left, right);
			
			if (!selection.isEmpty()) {
				final StringSelection transfer = new StringSelection(selection);
				
				Toolkit.getDefaultToolkit()
					.getSystemClipboard()
					.setContents(transfer, null);
				}
		});
	}
	
	private void removeSelection() {
		selectionLength = 0;
		selectionStart = caretPosition;
	}
	
	private void deleteSelection() {
		withSelectionLeftRight((left, right) -> {
			text.delete(left, right);
		});
		
		if (selectionLength > 0) {
			caretPosition -= selectionLength;
		}
		
		removeSelection();
	}
	
	private void moveCaretTo(final int position, final boolean select) {
		final int textLength = text.length();
		
		if (select && selectionLength == 0) {
			selectionStart = caretPosition;
		}
		
		caretPosition = Math.min(Math.max(position, 0), textLength);
		
		if (select) {
			selectionLength = caretPosition - selectionStart;
		} else {
			selectionLength = 0;
		}
		
		selectionLength = Math.min(Math.max(selectionLength, -selectionStart), textLength - selectionStart);
	}
	
	private void moveCaretBy(final int amount, final boolean select) {
		moveCaretTo(caretPosition + amount, select);
	}
	
	private void insert(final CharSequence s) {
		deleteSelection();
		
		text.insert(caretPosition, s);
		
		moveCaretBy(s.length(), false);
	}
	
	private void moveCaretToNextSpace(final boolean select) {
		final int textLength = text.length();
		
		if (caretPosition >= textLength) {
			return;
		}
		
		char rightChar = text.charAt(caretPosition);
		
		final boolean skipWhitespace = Character.isWhitespace(rightChar);
		
		while (caretPosition < textLength) {
			rightChar = text.charAt(caretPosition);
			
			final boolean isWhitespace = Character.isWhitespace(rightChar);
			
			if ((skipWhitespace && isWhitespace) || (!skipWhitespace && !isWhitespace)) {
				moveCaretBy(1, select);
			} else {
				break;
			}
		}
	}
	
	private void moveCaretToPreviousSpace(final boolean select) {
		if (caretPosition <= 0) {
			return;
		}
		
		char leftChar = text.charAt(caretPosition - 1);
		
		final boolean skipWhitespace = Character.isWhitespace(leftChar);
		
		while (caretPosition > 0) {
			leftChar = text.charAt(caretPosition - 1);
			
			final boolean isWhitespace = Character.isWhitespace(leftChar);
			
			if ((skipWhitespace && isWhitespace) || (!skipWhitespace && !isWhitespace)) {
				moveCaretBy(-1, select);
			} else {
				break;
			}
		}
	}
	
	private void insert(final char c) {
		deleteSelection();
		
		text.insert(caretPosition, c);
		
		moveCaretBy(1, false);
	}
	
	@Override
	protected void focusChangeEvent(final boolean focused) {
		;
	}
	
	@Override
	protected void hoverChangeEvent(final boolean hovered) {
		;
	}
	
	@Override
	public void drawText(final Graphics2D g, final int width, final int height) {
		final FontMetrics fm = g.getFontMetrics();
		final String text = getText();
		final int x = padding;
		final int y = height / 2 + fm.getAscent() / 2;
		final String textBeforeCaret = text.substring(0, caretPosition);
		final int caretX = padding + fm.stringWidth(textBeforeCaret);
		final int textOffset = Math.max(caretX - width + padding, 0);
		final Rectangle oldClip = g.getClipBounds();
		
		g.clipRect(padding, 0, width - padding * 2 + 1, height);
		g.translate(-textOffset, 0);
		g.drawString(text, x, y);
		
		if (selectionLength != 0) {
			withSelectionLeftRight((left, right) -> {
				final String leftString = text.substring(0, left);
				final String highlightedString = text.substring(left, right);
				final int selectionRectX = fm.stringWidth(leftString) + padding;
				final int selectionRectWidth = fm.stringWidth(highlightedString);
				
				g.setColor(selectionColor);
				g.fillRect(selectionRectX, padding, selectionRectWidth, height - padding * 2);
				g.setColor(foreground);
			});
		}
		
		if (isFocused()) {
			g.drawLine(caretX, padding, caretX, height - padding - 1);
		}
		
		g.translate(textOffset, 0);
		g.setClip(oldClip);
	}
	
	@Override
	public void mouseEvent(final int x, final int y, final int button, final boolean flag) {
		;
	}
	
	@Override
	public void mouseMotionEvent(final int x, final int y, final int modifiers) {
		;
	}
	
	@Override
	public void mouseWheelEvent(final int x, final int y, final int scroll) {
		;
	}
	
	@Override
	public void keyboardEvent(final int key, final char c, final int modifiers, final boolean flag) {
		if (!flag) {
			return;
		}
		
		final int textLength = text.length();
		final boolean shiftPressed = (modifiers & KeyEvent.SHIFT_DOWN_MASK) == KeyEvent.SHIFT_DOWN_MASK;
		final boolean ctrlPressed = (modifiers & KeyEvent.CTRL_DOWN_MASK) == KeyEvent.CTRL_DOWN_MASK;
		
		/* Yeah this is a mess */
		if (key == KeyEvent.VK_ENTER && action != null) {
			action.run();
		} else if (key == KeyEvent.VK_BACK_SPACE) {
			if (selectionLength == 0) {
				moveCaretBy(-1, true);
			}
			
			deleteSelection();
		} else if (key == KeyEvent.VK_DELETE) {
			if (selectionLength == 0) {
				moveCaretBy(1, true);
			}
			
			deleteSelection();
		} else if (key == KeyEvent.VK_LEFT) {
			if (ctrlPressed) {
				moveCaretToPreviousSpace(shiftPressed);
			} else {
				moveCaretBy(-1, shiftPressed);
			}
		} else if (key == KeyEvent.VK_RIGHT) {
			if (ctrlPressed) {
				moveCaretToNextSpace(shiftPressed);
			} else {
				moveCaretBy(1, shiftPressed);
			}
		} else if (key == KeyEvent.VK_HOME) {
			moveCaretTo(0, shiftPressed);
		} else if (key == KeyEvent.VK_END) {
			moveCaretTo(textLength, shiftPressed);
		} else if (key == KeyEvent.VK_V && ctrlPressed) {
			pasteClipboard();
		} else if (key == KeyEvent.VK_C && ctrlPressed) {
			copyToClipboard();
		} else if (key == KeyEvent.VK_A && ctrlPressed) {
			moveCaretTo(0, false);
			moveCaretTo(textLength, true);
		} else if (key == KeyEvent.VK_X && ctrlPressed) {
			copyToClipboard();
			deleteSelection();
		} else if (c != KeyEvent.CHAR_UNDEFINED && !Character.isISOControl(c)) {
			insert(c);
		}
	}
	
	/**
	 * Sets the overlay color of selected text.
	 * @param selectionColor The selection color
	 */
	public void setSeletionColor(final Color selectionColor) {
		this.selectionColor = selectionColor;
	}
	
	/**
	 * Sets the action to perform when enter is pressed.
	 * @param action The action
	 */
	public void setAction(final Runnable action) {
		this.action = action;
	}
	
	@Override
	public void setText(String newText) {
		if (newText == null) {
			newText = "";
		}
		
		text = new StringBuilder(newText);
		
		caretPosition = Math.min(text.length(), caretPosition);
		selectionLength = 0;
	}
	
	/**
	 * Returns the caret position.
	 * It is between 0 and the text length (both inclusive).
	 * @return The caret position
	 */
	public int getCaretPosition() {
		return caretPosition;
	}
	
	/**
	 * Returns text selection color.
	 * @return The selection color
	 */
	public Color getSelectionColor() {
		return selectionColor;
	}
	
	@Override
	public String getText() {
		return text.toString();
	}
	
}
