/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui.component;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.function.Consumer;

import de.sogomn.spjgl.ui.AbstractComponent;
import de.sogomn.spjgl.ui.IFontMetricsProvider;

/**
 * Slider component.
 * Can be dragged with the mouse.
 * @author Sogomn
 */
public final class Slider extends AbstractComponent {
	
	private int value;
	private int max;
	private boolean valueChanged;
	
	private int handlePos;
	private int handleSize;
	private Orientation orientation;
	private boolean snap;
	
	private Consumer<Integer> changeListener;
	
	private static final int DEFAULT_HANDLE_SIZE = 3;
	
	/**
	 * Creates a new slider with the given maximum value.
	 * The default orientation is horizontal, the default handle size is 3;
	 * @param max The maximum value
	 */
	public Slider(final int max) {
		this.max = Math.max(max, 1);
		
		handleSize = DEFAULT_HANDLE_SIZE;
		orientation = Orientation.HORIZONTAL;
	}
	
	private void updateValues(final int width, final int height) {
		final int size = orientation == Orientation.HORIZONTAL ? width : height;
		final int effectiveRange = size - handleSize;
		
		if (effectiveRange < 0) {
			value = max / 2;
			handlePos = 0;
			handleSize = size;
		} else if (!valueChanged) {
			handlePos = Math.min(Math.max(handlePos, 0), effectiveRange);
			
			final int newValue = (int)Math.round((double)handlePos * max / effectiveRange);
			
			if (newValue != value && changeListener != null) {
				value = newValue;
				
				changeListener.accept(value);
			}
		}
		
		if ((valueChanged || snap) && handleSize < size) {
			handlePos = value * effectiveRange / max;
			valueChanged = false;
		}
		
		value = Math.max(Math.min(value, max), 0);
	}
	
	@Override
	protected int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height) {
		return orientation == Orientation.HORIZONTAL ? max : 0;
	}
	
	@Override
	protected int calculatePreferredHeight(final IFontMetricsProvider fmp, final int width) {
		return orientation == Orientation.HORIZONTAL ? 0 : max;
	}
	
	@Override
	protected void focusChangeEvent(final boolean focused) {
		;
	}
	
	@Override
	protected void hoverChangeEvent(final boolean hovered) {
		;
	}
	
	@Override
	public void draw(final Graphics2D g, final int width, final int height) {
		updateValues(width, height);
		
		if (orientation == Orientation.HORIZONTAL) {
			g.fillRect(handlePos, 0, handleSize, height);
		} else {
			g.fillRect(0, handlePos, width, handleSize);
		}
	}
	
	@Override
	public void mouseEvent(final int x, final int y, final int button, final boolean flag) {
		handlePos = (orientation == Orientation.HORIZONTAL ? x : y) - handleSize / 2;
	}
	
	@Override
	public void mouseMotionEvent(final int x, final int y, final int modifiers) {
		final boolean leftPressed = (modifiers & MouseEvent.BUTTON1_DOWN_MASK) == MouseEvent.BUTTON1_DOWN_MASK;
		
		if (isFocused() && leftPressed) {
			handlePos = (orientation == Orientation.HORIZONTAL ? x : y) - handleSize / 2;
		}
	}
	
	@Override
	public void mouseWheelEvent(final int x, final int y, final int rotation) {
		;
	}
	
	@Override
	public void keyboardEvent(final int key, final char c, final int modifiers, final boolean flag) {
		;
	}
	
	/**
	 * Sets the current slider value.
	 * Will be clamped if out of range.
	 * @param value The new value
	 */
	public void setValue(final int value) {
		this.value = Math.min(Math.max(value, 0), max);
		
		valueChanged = true;
	}
	
	/**
	 * Sets the maximum slider value.
	 * @param max The new maximum value
	 */
	public void setMax(final int max) {
		this.max = Math.min(max, 1);
	}
	
	/**
	 * Sets the slider orientation.
	 * @param orientation The new orientation
	 */
	public void setOrientation(final Orientation orientation) {
		this.orientation = orientation;
	}
	
	/**
	 * Sets the slider handle size.
	 * @param handleSize The new handle size
	 */
	public void setHandleSize(final int handleSize) {
		this.handleSize = handleSize;
	}
	
	/**
	 * Sets whether or not the slider handle should snap onto values.
	 * @param snap True to snap; false otherwise
	 */
	public void setSnap(final boolean snap) {
		this.snap = snap;
	}
	
	/**
	 * Sets the change listener which is notified when the value has been changed by dragging the slider handle.
	 * @param changeListener The new change listener
	 */
	public void setChangeListener(final Consumer<Integer> changeListener) {
		this.changeListener = changeListener;
	}
	
	/**
	 * Returns the current slider value.
	 * Always between 0 (inclusive) and max (inclusive).
	 * @return The current slider value
	 */
	public int getValue() {
		return value;
	}
	
	/**
	 * Returns the maximum slider value.
	 * @return The maximum slider value
	 */
	public int getMax() {
		return max;
	}
	
	/**
	 * Returns the slider orientation.
	 * @return The orientation
	 */
	public Orientation getOrientation() {
		return orientation;
	}
	
	/**
	 * Returns the slider handle size.
	 * @return The handle size
	 */
	public int getHandleSize() {
		return handleSize;
	}
	
	/**
	 * Returns whether or not the slider handle snaps onto values.
	 * @return True if snap is enabled; false otherwise
	 */
	public boolean getSnap() {
		return snap;
	}
	
	/**
	 * Returns the current change listener.
	 * @return The current change listener
	 */
	public Consumer<Integer> getChangeListener() {
		return changeListener;
	}
	
	/**
	 * Holds different orientations for the slider component.
	 */
	public static enum Orientation {
		
		/**
		 * Vertical orientation (left to right).
		 */
		VERTICAL,
		
		/**
		 * Horizontal orientation (top to bottom).
		 */
		HORIZONTAL;
		
	}
	
}
