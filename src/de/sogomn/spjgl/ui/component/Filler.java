/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui.component;

import java.awt.Graphics2D;

import de.sogomn.spjgl.ui.AbstractComponent;
import de.sogomn.spjgl.ui.IFontMetricsProvider;

/**
 * A filler component that does not have any functionality.
 * @author Sogomn
 */
public final class Filler extends AbstractComponent {
	
	/**
	 * Creates a new filler.
	 */
	public Filler() {
		;
	}
	
	/**
	 * Creates a new filler and sets its preferred size to the specified one.
	 * @param width The preferred width
	 * @param height The preferred height
	 */
	public Filler(final int width, final int height) {
		setPreferredSize(width, height);
	}
	
	@Override
	protected int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height) {
		return 0;
	}
	
	@Override
	protected int calculatePreferredHeight(final IFontMetricsProvider fmp, final int width) {
		return 0;
	}
	
	@Override
	protected void focusChangeEvent(final boolean focused) {
		;
	}
	
	@Override
	protected void hoverChangeEvent(final boolean hovered) {
		;
	}
	
	@Override
	public void draw(final Graphics2D g, final int width, final int height) {
		;
	}
	
	@Override
	public void mouseEvent(final int x, final int y, final int button, final boolean flag) {
		;
	}
	
	@Override
	public void mouseMotionEvent(final int x, final int y, final int modifiers) {
		;
	}
	
	@Override
	public void mouseWheelEvent(final int x, final int y, final int rotation) {
		;
	}
	
	@Override
	public void keyboardEvent(final int key, final char c, final int modifiers, final boolean flag) {
		;
	}
	
}
