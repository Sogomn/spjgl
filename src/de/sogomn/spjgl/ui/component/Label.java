/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui.component;

import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import de.sogomn.spjgl.ui.IFontMetricsProvider;

/**
 * A text label.
 * Does support multiple lines.
 * @author Sogomn
 */
public final class Label extends AbstractTextComponent<List<String>> {
	
	private Alignment alignment;
	
	private static final String LINE_SPLIT_REGEX = "\\r?\\n";
	private static final Pattern WORD_SPLIT_REGEX = Pattern.compile("(\\S+\\s*)");
	
	/**
	 * Creates a new label component.
	 * @param text The text to display
	 * @param alignment The text alignment
	 */
	public Label(final String text, final Alignment alignment) {
		super(Arrays.asList(text.split(LINE_SPLIT_REGEX)));
		
		this.alignment = alignment;
	}
	
	/**
	 * Creates a new label component.
	 * @param text The text to display
	 */
	public Label(final String text) {
		this(text, Alignment.LEFT);
	}
	
	/**
	 * Creates a new label component with empty text.
	 */
	public Label() {
		this("");
	}
	
	private List<String> breakLines(final FontMetrics fm, final int width) {
		final int widthAvailable = width - padding * 2;
		final LinkedList<String> newLines = new LinkedList<String>();
		final StringBuilder lineBuilder = new StringBuilder();
		
		for (final String line : text) {
			final Matcher wordMatcher = WORD_SPLIT_REGEX.matcher(line);
			
			while (wordMatcher.find()) {
				final String word = wordMatcher.group();
				final int lineBuilderLength = lineBuilder.length();
				
				lineBuilder.append(word);
				
				final int lineLength = fm.stringWidth(lineBuilder.toString());
				
				if (lineLength > widthAvailable && lineBuilderLength > 0) {
					lineBuilder.setLength(lineBuilderLength);
					newLines.add(lineBuilder.toString());
					lineBuilder.setLength(0);
					lineBuilder.append(word);
				}
			}
			
			newLines.add(lineBuilder.toString());
			lineBuilder.setLength(0);
		}
		
		return newLines;
	}
	
	@Override
	protected void focusChangeEvent(final boolean focused) {
		;
	}
	
	@Override
	protected void hoverChangeEvent(final boolean hovered) {
		;
	}
	
	@Override
	protected int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height) {
		final FontMetrics fm = fmp.getFontMetrics(font);
		final int textWidth = text.stream()
			.mapToInt(fm::stringWidth)
			.max()
			.orElse(0);
		
		return textWidth + padding * 2;
	}
	
	@Override
	protected int calculatePreferredHeight(final IFontMetricsProvider fmp, final int width) {
		final FontMetrics fm = fmp.getFontMetrics(font);
		final List<String> lines = breakLines(fm, width);
		
		return fm.getHeight() * Math.max(lines.size(), 1) + padding * 2;
	}
	
	@Override
	public void drawText(final Graphics2D g, final int width, final int height) {
		final FontMetrics fm = g.getFontMetrics();
		final List<String> lines = breakLines(fm, width);
		final int lineCount = lines.size();
		final int evenOffset = lineCount % 2 == 0 ? fm.getHeight() / 2 : 0;
		
		for (int i = 0; i < lineCount; i++) {
			final String line = lines.get(i);
			final int x;
			final int y = height / 2 + fm.getAscent() / 2 + fm.getHeight() * (i - lineCount / 2) + evenOffset;
			
			if (alignment == Alignment.LEFT) {
				x = padding;
			} else if (alignment == Alignment.RIGHT) {
				x = width - padding - fm.stringWidth(line);
			} else {
				x = width / 2 - fm.stringWidth(line) / 2;
			}
			
			g.drawString(line, x, y);
		}
	}
	
	@Override
	public void mouseEvent(final int x, final int y, final int button, final boolean flag) {
		;
	}
	
	@Override
	public void mouseMotionEvent(final int x, final int y, final int modifiers) {
		;
	}
	
	@Override
	public void mouseWheelEvent(final int x, final int y, final int rotation) {
		;
	}
	
	@Override
	public void keyboardEvent(final int key, final char c, final int modifiers, final boolean flag) {
		;
	}
	
	@Override
	public void setText(final String s) {
		final String[] lines = s.split(LINE_SPLIT_REGEX);
		
		text = Arrays.asList(lines);
	}
	
	/**
	 * Sets the text alignment for the label.
	 * @param alignment The new text alignment
	 */
	public void setAlignment(final Alignment alignment) {
		this.alignment = alignment;
	}
	
	@Override
	public String getText() {
		return text.stream().collect(Collectors.joining(System.lineSeparator()));
	}
	
	/**
	 * Returns the current text alignment.
	 * @return The text alignment
	 */
	public Alignment getAlignment() {
		return alignment;
	}
	
	/**
	 * An enum that holds parameters for text alignment.
	 */
	public static enum Alignment {
		
		/**
		 * Left-aligned text.
		 */
		LEFT,
		
		/**
		 * Centered text.
		 */
		CENTER,
		
		/**
		 * Right-aligned text.
		 */
		RIGHT;
		
		Alignment() {
			;
		}
		
	}
	
}
