/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui;

import java.util.OptionalInt;

import de.sogomn.spjgl.IKeyboardListener;
import de.sogomn.spjgl.IMouseListener;

/**
 * An base class for all UI components.
 * Components can be added to {@link de.sogomn.spjgl.ui.AbstractContainer}s.
 * The same component should only be added to one contaianer, not multiple.
 * @author Sogomn
 */
public abstract class AbstractComponent implements ISizedDrawable, IMouseListener, IKeyboardListener {
	
	private AbstractContainer<?> parent;
	
	private OptionalInt fixedPreferredWidth, fixedPreferredHeight;
	
	private boolean focused, hovered;
	
	/**
	 * Creates a new UI component.
	 */
	public AbstractComponent() {
		fixedPreferredWidth = OptionalInt.empty();
		fixedPreferredHeight = OptionalInt.empty();
	}
	
	void setParent(final AbstractContainer<?> parent) {
		this.parent = parent;
	}
	
	void setFocused(final boolean newState) {
		if (focused != newState) {
			focused = newState;
			
			focusChangeEvent(focused);
		}
	}
	
	void setHovered(final boolean newState) {
		if (hovered != newState) {
			hovered = newState;
			
			hoverChangeEvent(hovered);
		}
	}
	
	void propagateDirty(final AbstractComponent source) {
		if (parent != null && source != parent) {
			parent.propagateDirty(this);
		}
	}
	
	/**
	 * Called when the focus on this component changes.
	 * @param focused The new focused state
	 */
	protected abstract void focusChangeEvent(final boolean focused);
	
	/**
	 * Called when this component's hover status changes.
	 * @param hovered The new hovered state
	 */
	protected abstract void hoverChangeEvent(final boolean hovered);
	
	/**
	 * Calculates the preferred width of this component based on a font context and a given height.
	 * @param fmp The font metrics provier which returns a FontMetrics object based on a specified font
	 * @param height The height to base the calculation on
	 * @return The preferred width of this component
	 */
	protected abstract int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height);
	
	/**
	 * Calculates the preferred height of this component based on a font context and a given width.
	 * @param fmp The font metrics provier which returns a FontMetrics object based on a specified font
	 * @param width The width to base the calculation on
	 * @return The preferred height of this component
	 */
	protected abstract int calculatePreferredHeight(final IFontMetricsProvider fmp, final int width);
	
	/**
	 * Returns the component's preferred width based on a font metrics provider and a height.
	 * If a preferred width has been set manually, it is returned instead.
	 * @param fmp The font metrics provier which returns a FontMetrics object based on a specified font
	 * @param height The height the preferred width is relative to
	 * @return The preferred width of this component based on the height
	 */
	public final int getPreferredWidth(final IFontMetricsProvider fmp, final int height) {
		return fixedPreferredWidth.orElseGet(() -> calculatePreferredWidth(fmp, height));
	}
	
	/**
	 * Returns the component's preferred height based on a font metrics provider and a width.
	 * If a preferred height has been set manually, it is returned instead.
	 * @param fmp The font metrics provier which returns a FontMetrics object based on a specified font
	 * @param width The width the preferred height is relative to
	 * @return The preferred height of this component based on the width
	 */
	public final int getPreferredHeight(final IFontMetricsProvider fmp, final int width) {
		return fixedPreferredHeight.orElseGet(() -> calculatePreferredHeight(fmp, width));
	}
	
	/**
	 * Sets the preferred width of this component.
	 * @param width The new preferred width
	 */
	public final void setPreferredWidth(final int width) {
		fixedPreferredWidth = OptionalInt.of(width);
		
		updateLayout();
	}
	
	/**
	 * Sets the preferred height of this component.
	 * @param height The new preferred height
	 */
	public final void setPreferredHeight(final int height) {
		fixedPreferredHeight = OptionalInt.of(height);
		
		updateLayout();
	}
	
	/**
	 * Sets the preferred size of this component.
	 * @param width The new preferred width
	 * @param height The new preferred height
	 */
	public final void setPreferredSize(final int width, final int height) {
		setPreferredWidth(width);
		setPreferredHeight(height);
	}
	
	/**
	 * Makes the component forget that a preferred width has been set manually.
	 */
	public final void unsetPreferredWidth() {
		fixedPreferredWidth = OptionalInt.empty();
		
		updateLayout();
	}
	
	/**
	 * Makes the component forget that a preferred width has been set manually.
	 */
	public final void unsetPreferredHeight() {
		fixedPreferredHeight = OptionalInt.empty();
		
		updateLayout();
	}
	
	/**
	 * Makes the component forget that a preferred size has been set manually.
	 */
	public final void unsetPreferredSize() {
		unsetPreferredWidth();
		unsetPreferredHeight();
	}
	
	/**
	 * Informs all parent containers that this component's size needs to be update.
	 * Should be called whenever the calculation of {@link #calculatePreferredWidth} or {@link #calculatePreferredHeight} changes.
	 * Gets called automatically after a preferred size has been set manually.
	 */
	public final void updateLayout() {
		propagateDirty(null);
	}
	
	/**
	 * Returns whether or not this component is currently focused or not.
	 * @return The state
	 */
	public final boolean isFocused() {
		return focused;
	}
	
	/**
	 * Returns whether or not this component is currently hovered or not.
	 * @return The state
	 */
	public final boolean isHovered() {
		return hovered;
	}
	
}
