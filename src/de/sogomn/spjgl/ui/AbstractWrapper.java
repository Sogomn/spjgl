/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui;

import java.awt.Graphics2D;

/**
 * A wrapper component to hold exactly one other component.
 * Passes all events to the inner component.
 * @author Sogomn
 */
public abstract class AbstractWrapper extends AbstractComponent {
	
	/**
	 * The inner component.
	 */
	protected AbstractComponent inner;
	
	/**
	 * Constructs a new wrapper around a component.
	 * @param inner The inner component
	 */
	public AbstractWrapper(final AbstractComponent inner) {
		this.inner = inner;
	}
	
	@Override
	void setParent(final AbstractContainer<?> parent) {
		super.setParent(parent);
		
		if (inner != null) {
			inner.setParent(parent);
		}
	}
	
	@Override
	void setFocused(final boolean focused) {
		super.setFocused(focused);
		
		if (inner != null) {
			inner.setFocused(focused);
		}
	}
	
	@Override
	void setHovered(final boolean hovered) {
		super.setHovered(hovered);
		
		if (inner != null) {
			inner.setHovered(hovered);
		}
	}
	
	@Override
	void propagateDirty(final AbstractComponent source) {
		super.propagateDirty(source);
		
		inner.propagateDirty(source);
	}
	
	@Override
	protected void focusChangeEvent(final boolean focused) {
		if (inner != null) {
			inner.focusChangeEvent(focused);
		}
	}
	
	@Override
	protected void hoverChangeEvent(final boolean hovered) {
		if (inner != null) {
			inner.hoverChangeEvent(hovered);
		}
	}
	
	@Override
	protected int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height) {
		if (inner != null) {
			return inner.getPreferredWidth(fmp, height);
		}
		
		return 0;
	}
	
	@Override
	protected int calculatePreferredHeight(final IFontMetricsProvider fmp, final int width) {
		if (inner != null) {
			return inner.getPreferredHeight(fmp, width);
		}
		
		return 0;
	}
	
	@Override
	public void draw(final Graphics2D g, final int width, final int height) {
		if (inner != null) {
			inner.draw(g, width, height);
		}
	}
	
	@Override
	public void mouseEvent(final int x, final int y, final int button, final boolean flag) {
		if (inner != null) {
			inner.mouseEvent(x, y, button, flag);
		}
	}
	
	@Override
	public void mouseMotionEvent(final int x, final int y, final int modifiers) {
		if (inner != null) {
			inner.mouseMotionEvent(x, y, modifiers);
		}
	}
	
	@Override
	public void mouseWheelEvent(final int x, final int y, final int rotation) {
		if (inner != null) {
			inner.mouseWheelEvent(x, y, rotation);
		}
	}
	
	@Override
	public void keyboardEvent(final int key, final char c, final int modifiers, final boolean flag) {
		if (inner != null) {
			inner.keyboardEvent(key, c, modifiers, flag);
		}
	}
	
	/**
	 * Sets the inner component of this wrapper.
	 * @param inner The new inner component
	 */
	public final void setInner(final AbstractComponent inner) {
		this.inner = inner;
	}
	
	/**
	 * Returns the inner component of this wrapper.
	 * @return The inner component
	 */
	public final AbstractComponent getInner() {
		return inner;
	}
	
}
