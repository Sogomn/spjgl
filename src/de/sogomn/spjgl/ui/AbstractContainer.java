/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A container to hold other components.
 * Containers are components themselves so containers can contain containers.
 * @author Sogomn
 * @param <LayoutParam> The type used to specify how the container should position the components
 */
public abstract class AbstractContainer<LayoutParam> extends AbstractComponent {
	
	private ArrayList<AbstractComponent> components;
	private ArrayList<LayoutParam> params;
	private List<ComponentConstraints> constraints;
	
	private boolean dirty;
	
	private AbstractComponent focus, hoverFocus;
	
	/**
	 * Constructs a new container.
	 */
	public AbstractContainer() {
		components = new ArrayList<AbstractComponent>();
		params = new ArrayList<LayoutParam>();
	}
	
	private void doLayout(final int width, final int height, final IFontMetricsProvider fmp) {
		final List<AbstractComponent> componentsImmutable = Collections.unmodifiableList(components);
		final List<LayoutParam> paramsImmutable = Collections.unmodifiableList(params);
		
		constraints = layout(componentsImmutable, paramsImmutable, width, height, fmp);
		dirty = false;
	}
	
	@Override
	void propagateDirty(final AbstractComponent source) {
		dirty = true;
		
		components.stream()
			.filter(c -> c != source)
			.forEach(c -> c.propagateDirty(this));
		
		super.propagateDirty(source);
	}
	
	/**
	 * Calculates the layout of this container.
	 * @param components An immutable list of all components this container holds
	 * @param params An immutable list of all components' parameters
	 * @param width The width of the layout
	 * @param height The height of the layout
	 * @param fmp The font metrics provider
	 * @return A list containing information about how to position the components; must be the same size as the components list
	 */
	protected abstract List<ComponentConstraints> layout(final List<AbstractComponent> components, final List<LayoutParam> params, final int width, final int height, final IFontMetricsProvider fmp);
	
	/**
	 * Calculates the preferred width of this container.
	 * @param fmp The font metrics provider
	 * @param height The height to base the calculation on
	 * @param components An immutable list of all components
	 * @param params An immutable list of all component parameter
	 * @return The preferred width of this container
	 */
	protected abstract int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height, final List<AbstractComponent> components, final List<LayoutParam> params);
	
	/**
	 * Calculates the preferred height of this container.
	 * @param fmp The font metrics provider
	 * @param width The width to base the calculation on
	 * @param components An immutable list of all components
	 * @param params An immutable list of all component parameter
	 * @return The preferred height of this container
	 */
	protected abstract int calculatePreferredHeight(final IFontMetricsProvider fmp, final int width, final List<AbstractComponent> components, final List<LayoutParam> params);
	
	@Override
	protected void focusChangeEvent(final boolean focused) {
		if (!focused && focus != null) {
			focus.setFocused(false);
		}
	}
	
	@Override
	protected void hoverChangeEvent(final boolean hovered) {
		if (!hovered && hoverFocus != null) {
			hoverFocus.setHovered(false);
		}
	}
	
	@Override
	protected final int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height) {
		final List<AbstractComponent> componentsImmutable = Collections.unmodifiableList(components);
		final List<LayoutParam> paramsImmutable = Collections.unmodifiableList(params);
		
		return calculatePreferredWidth(fmp, height, componentsImmutable, paramsImmutable);
	}
	
	@Override
	protected final int calculatePreferredHeight(final IFontMetricsProvider fmp, final int width) {
		final List<AbstractComponent> componentsImmutable = Collections.unmodifiableList(components);
		final List<LayoutParam> paramsImmutable = Collections.unmodifiableList(params);
		
		return calculatePreferredHeight(fmp, width, componentsImmutable, paramsImmutable);
	}
	
	@Override
	public void draw(final Graphics2D g, final int width, final int height) {
		if (dirty) {
			doLayout(width, height, g::getFontMetrics);
		}
		
		for (int i = 0; i < components.size(); i++) {
			final AbstractComponent c = components.get(i);
			final ComponentConstraints cons = constraints.get(i);
			
			g.translate(cons.x, cons.y);
			c.draw(g, cons.width, cons.height);
			g.translate(-cons.x, -cons.y);
		}
	}
	
	@Override
	public void mouseEvent(final int x, final int y, final int button, final boolean flag) {
		if (dirty) {
			return;
		}
		
		for (int i = components.size() - 1; i >= 0 ; i--) {
			final AbstractComponent c = components.get(i);
			final ComponentConstraints cons = constraints.get(i);
			final int relativeX = x - cons.x;
			final int relativeY = y - cons.y;
			
			if (relativeX > 0 && relativeY > 0 && relativeX < cons.width && relativeY < cons.height) {
				if (focus != null && focus != c) {
					focus.setFocused(false);
				}
				
				focus = c;
				
				c.setFocused(true);
				c.mouseEvent(relativeX, relativeY, button, flag);
				
				break;
			} else {
				c.setFocused(false);
			}
		}
	}
	
	@Override
	public void mouseMotionEvent(final int x, final int y, final int modifiers) {
		if (dirty) {
			return;
		}
		
		for (int i = components.size() - 1; i >= 0 ; i--) {
			final AbstractComponent c = components.get(i);
			final ComponentConstraints cons = constraints.get(i);
			final int relativeX = x - cons.x;
			final int relativeY = y - cons.y;
			
			if (relativeX > 0 && relativeY > 0 && relativeX < cons.width && relativeY < cons.height) {
				if (hoverFocus != null && hoverFocus != c) {
					hoverFocus.setHovered(false);
				}
				
				hoverFocus = c;
				
				c.setHovered(true);
				c.mouseMotionEvent(relativeX, relativeY, modifiers);
				
				break;
			} else {
				c.setHovered(false);
			}
		}
	}
	
	@Override
	public void mouseWheelEvent(final int x, final int y, final int rotation) {
		if (dirty) {
			return;
		}
		
		for (int i = components.size() - 1; i >= 0 ; i--) {
			final AbstractComponent c = components.get(i);
			final ComponentConstraints cons = constraints.get(i);
			final int relativeX = x - cons.x;
			final int relativeY = y - cons.y;
			
			if (relativeX > 0 && relativeY > 0 && relativeX < cons.width && relativeY < cons.height) {
				c.mouseWheelEvent(relativeX, relativeY, rotation);
				
				break;
			}
		}
	}
	
	@Override
	public void keyboardEvent(final int key, final char c, final int modifiers, final boolean flag) {
		if (dirty || focus == null) {
			return;
		}
		
		focus.keyboardEvent(key, c, modifiers, flag);
	}
	
	/**
	 * Adds a component to this container
	 * @param component The component to add
	 * @param param The parameters for the component
	 */
	public void add(final AbstractComponent component, final LayoutParam param) {
		insert(component, param, components.size());
	}
	
	/**
	 * Inserts a component into this container.
	 * @param component The component to insert
	 * @param param The parameters for the component
	 * @param index The index to insert the component at
	 */
	public void insert(final AbstractComponent component, final LayoutParam param, final int index) {
		component.setParent(this);
		components.add(index, component);
		params.add(index, param);
		
		updateLayout();
	}
	
	/**
	 * Removes a component from this container.
	 * @param component The component to remove
	 * @return The old index of the removed component or -1
	 */
	public int remove(final AbstractComponent component) {
		final int idx = components.indexOf(component);
		
		if (idx != -1) {
			components.remove(idx);
			params.remove(idx);
			component.setParent(null);
			
			updateLayout();
		}
		
		return idx;
	}
	
	/**
	 * Returns an immutable list of all components this container holds.
	 * @return The list of components
	 */
	public final List<AbstractComponent> getComponents() {
		if (dirty) {
			return null;
		}
		
		final List<AbstractComponent> componentsImmutable = Collections.unmodifiableList(components);
		
		return componentsImmutable;
	}
	
	/**
	 * Returns the current position and size of the given component.
	 * Is null if either the container is dirty or the container does not contain the component.
	 * @param component The component to check the constraints for
	 * @return The component constraints or null
	 */
	public final ComponentConstraints getConstraints(final AbstractComponent component) {
		if (dirty) {
			return null;
		}
		
		final int idx = components.indexOf(component);
		
		if (idx != -1) {
			return constraints.get(idx);
		}
		
		return null;
	}
	
	/**
	 * A data class that represents position and size of a component.
	 */
	public static final class ComponentConstraints {
		
		/**
		 * The x and y position of this component.
		 */
		public final int x, y;
		
		/**
		 * The width and height of this component.
		 */
		public final int width, height;
		
		/**
		 * Creates a new ComponentConstraints object.
		 * @param x The x position
		 * @param y The y position
		 * @param width The width
		 * @param height The height
		 */
		public ComponentConstraints(final int x, final int y, final int width, final int height) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}
		
	}
	
}
