/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui;

import java.awt.Font;
import java.awt.FontMetrics;

/**
 * Defines an interface to retrieve a FontMetrics object based on a font.
 * @author Sogomn
 */
public interface IFontMetricsProvider {
	
	/**
	 * Returns a FontMetrics object based on the specified font.
	 * @param font The font
	 * @return The FontMetrics object
	 */
	FontMetrics getFontMetrics(final Font font);
	
}
