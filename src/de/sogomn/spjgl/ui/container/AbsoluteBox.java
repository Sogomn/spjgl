/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui.container;

import java.util.List;

import de.sogomn.spjgl.ui.AbstractComponent;
import de.sogomn.spjgl.ui.AbstractContainer;
import de.sogomn.spjgl.ui.IFontMetricsProvider;
import de.sogomn.spjgl.ui.AbstractContainer.ComponentConstraints;

/**
 * Container that positions its elements based on absolute values.
 * @author Sogomn
 */
public final class AbsoluteBox extends AbstractContainer<ComponentConstraints> {
	
	/**
	 * Creates a new AbsoluteBox.
	 */
	public AbsoluteBox() {
		;
	}
	
	@Override
	protected List<ComponentConstraints> layout(final List<AbstractComponent> components, final List<ComponentConstraints> params, final int width, final int height, final IFontMetricsProvider fmp) {
		return params;
	}
	
	@Override
	protected int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height, final List<AbstractComponent> components, final List<ComponentConstraints> params) {
		return params.stream()
			.mapToInt(c -> c.x + c.width)
			.max()
			.orElse(0);
	}
	
	@Override
	protected int calculatePreferredHeight(final IFontMetricsProvider fmp, final int height, final List<AbstractComponent> components, final List<ComponentConstraints> params) {
		return params.stream()
			.mapToInt(c -> c.y + c.height)
			.max()
			.orElse(0);
	}
	
}
