/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl.ui.container;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import de.sogomn.spjgl.ui.AbstractComponent;
import de.sogomn.spjgl.ui.AbstractContainer;
import de.sogomn.spjgl.ui.IFontMetricsProvider;
import de.sogomn.spjgl.ui.container.FlowBox.FlowParam;

/**
 * A container that positions the components in a row or column.
 * Grows and shrinks them based on parameters and space available.
 * @author Sogomn
 */
public final class FlowBox extends AbstractContainer<FlowParam> {
	
	private FlowDirection direction;
	private int innerMargin, outerMargin;
	
	/**
	 * Constructs a new FlowBox.
	 * @param direction The flow direction
	 */
	public FlowBox(final FlowDirection direction) {
		this(direction, 0, 0);
	}
	
	/**
	 * Constructs a new FlowBox.
	 * @param direction The direction
	 * @param innerMargin The inner margin between the components
	 * @param outerMargin The outer margin around all componentw
	 */
	public FlowBox(final FlowDirection direction, final int innerMargin, final int outerMargin) {
		this.direction = direction;
		this.innerMargin = innerMargin;
		this.outerMargin = outerMargin;
	}
	
	@Override
	protected List<ComponentConstraints> layout(final List<AbstractComponent> components, final List<FlowParam> params, final int width, final int height, final IFontMetricsProvider fmp) {
		final int len = Math.min(components.size(), params.size());
		final ArrayList<ComponentConstraints> constraints = new ArrayList<ComponentConstraints>(len);
		final int totalSize;
		final ToIntFunction<AbstractComponent> preferredSizeFunction;
		final ToIntFunction<ComponentConstraints> mainSizeFunction;
		final ToIntFunction<ComponentConstraints> mainPosFunction;
		final BiFunction<Integer, Integer, ComponentConstraints> constraintsProducer;
		
		/* Set up variables and functions based on which flow direction we're using */
		if (direction == FlowDirection.RIGHT) {
			totalSize = width;
			preferredSizeFunction = c -> c.getPreferredWidth(fmp, height - outerMargin * 2);
			mainSizeFunction = c -> c.width;
			mainPosFunction = c -> c.x;
			constraintsProducer = (mainPos, mainSize) -> new ComponentConstraints(mainPos, outerMargin, mainSize, height - outerMargin * 2);
		} else {
			totalSize = height;
			preferredSizeFunction = c -> c.getPreferredHeight(fmp, width - outerMargin * 2);
			mainSizeFunction = c -> c.height;
			mainPosFunction = c -> c.y;
			constraintsProducer = (mainPos, mainSize) -> new ComponentConstraints(outerMargin, mainPos, width - outerMargin * 2, mainSize);
		}
		
		final List<Integer> preferredSizes = components.stream()
			.map(c -> preferredSizeFunction.applyAsInt(c))
			.collect(Collectors.toList());
		final int sizeRequired = preferredSizes.stream()
			.mapToInt(Integer::valueOf)
			.sum();
		
		int sizeLeft = Math.max(totalSize - innerMargin * (len - 1) - outerMargin * 2, 0);
		int numFillComponents = 0;
		int currentFlowPos = outerMargin;
		
		final boolean overflow = sizeRequired > sizeLeft;
		
		/*
		 * If we have less space than we would like, shrink the components that aren't fixed
		 * Else, grow the fill components
		 */
		if (overflow) {
			final int sizeFixed = IntStream.range(0, len)
				.filter(i -> params.get(i) == FlowParam.COMPACT_FIXED)
				.map(preferredSizes::get)
				.sum();
			final int sizeMissing = sizeRequired - sizeLeft;
			
			for (int i = 0; i < len; i++) {
				final int preferredSize = preferredSizes.get(i);
				final double weight = (double)preferredSize / (sizeRequired - sizeFixed);
				final int sizeCut = (int)(sizeMissing * weight);
				final int componentSize;
				
				/* Either enforce the preferred size or shrink the component based on their preferred size */
				if (params.get(i) == FlowParam.COMPACT_FIXED) {
					componentSize = preferredSize;
				} else {
					componentSize = Math.max(preferredSize - sizeCut, 0);
				}
				
				final ComponentConstraints cons = constraintsProducer.apply(currentFlowPos, componentSize);
				
				constraints.add(cons);
				
				currentFlowPos += componentSize + innerMargin;
			}
		} else {
			/* At this point we have enough space availble for all components */
			for (int i = 0; i < len; i++) {
				final FlowParam param = params.get(i);
				final int componentSize;
				
				/* Set compact components to their preferred size and count the fill components */
				if (param == FlowParam.COMPACT || param == FlowParam.COMPACT_FIXED) {
					componentSize = Math.max(preferredSizes.get(i), 0);
					sizeLeft -= componentSize;
				} else {
					componentSize = 0;
					numFillComponents++;
				}
				
				final ComponentConstraints cons = constraintsProducer.apply(0, componentSize);
				
				constraints.add(cons);
			}
			
			/* Distribute the size for fill components equally */
			final int fillSize = sizeLeft / Math.max(numFillComponents, 1);
			
			for (int i = 0; i < len; i++) {
				final FlowParam param = params.get(i);
				
				if (param == FlowParam.FILL) {
					final ComponentConstraints newCons = constraintsProducer.apply(0, fillSize);
					
					constraints.set(i, newCons);
				}
			}
			
			/* Now that we know all the component sizes, calculate their positions */
			for (int i = 0; i < len; i++) {
				final ComponentConstraints currentCons = constraints.get(i);
				final int mainSize = mainSizeFunction.applyAsInt(currentCons);
				final ComponentConstraints newCons = constraintsProducer.apply(currentFlowPos, mainSize);
				
				constraints.set(i, newCons);
				
				currentFlowPos += mainSize + innerMargin;
			}
		}
		
		/* This is to ensure that we're still pixel-perfect, so we just stretch the last component (in practice usually only a few pixels) */
		if (len > 0 && (numFillComponents > 0 || overflow)) {
			final int spaceLeft = totalSize - outerMargin - currentFlowPos + innerMargin;
			final ComponentConstraints currentCons = constraints.get(len - 1);
			final int mainPos = mainPosFunction.applyAsInt(currentCons);
			final int mainSize = mainSizeFunction.applyAsInt(currentCons);
			final ComponentConstraints newCons = constraintsProducer.apply(mainPos, mainSize + spaceLeft);
			
			constraints.set(len - 1, newCons);
		}
		
		return constraints;
	}
	
	@Override
	protected int calculatePreferredWidth(final IFontMetricsProvider fmp, final int height, final List<AbstractComponent> components, final List<FlowParam> params) {
		final List<ComponentConstraints> cons = layout(components, params, Integer.MAX_VALUE, height, fmp);
		final int len = Math.min(components.size(), cons.size());
		final IntStream widths = IntStream.range(0, len).map(i -> {
			final AbstractComponent component = components.get(i);
			final ComponentConstraints c = cons.get(i);
			
			return component.getPreferredWidth(fmp, c.height);
		});
		final int componentHeight;
		
		if (direction == FlowDirection.RIGHT) {
			componentHeight = widths.sum() + innerMargin * Math.max(components.size() - 1, 0);
		} else {
			componentHeight = widths.max().orElse(0);
		}
		
		return componentHeight + outerMargin * 2;
	}
	
	@Override
	protected int calculatePreferredHeight(final IFontMetricsProvider fmp, final int width, final List<AbstractComponent> components, List<FlowParam> params) {
		final List<ComponentConstraints> cons = layout(components, params, width, Integer.MAX_VALUE, fmp);
		final int len = Math.min(components.size(), cons.size());
		final IntStream heights = IntStream.range(0, len).map(i -> {
			final AbstractComponent component = components.get(i);
			final ComponentConstraints c = cons.get(i);
			
			return component.getPreferredHeight(fmp, c.width);
		});
		final int componentWidth;
		
		if (direction == FlowDirection.RIGHT) {
			componentWidth = heights.max().orElse(0);
		} else {
			componentWidth = heights.sum() + innerMargin * Math.max(components.size() - 1, 0);
		}
		
		return componentWidth + outerMargin * 2;
	}
	
	/**
	 * Sets the inner margin of the FlowBox.
	 * @param innerMargin The new inner margin
	 */
	public void setInnerMargin(final int innerMargin) {
		this.innerMargin = innerMargin;
		
		updateLayout();
	}
	
	/**
	 * Sets the outer margin of the FlowBox.
	 * @param outerMargin The new outer margin
	 */
	public void setOuterMargin(final int outerMargin) {
		this.outerMargin = outerMargin;
		
		updateLayout();
	}
	
	/**
	 * Returns the inner margin.
	 * @return The inner margin
	 */
	public int getInnerMargin() {
		return innerMargin;
	}
	
	/**
	 * Returns the outer margin.
	 * @return The outer margin
	 */
	public int getOuterMargin() {
		return outerMargin;
	}
	
	/**
	 * An enum that holds the flow directions of a FlowBox.
	 */
	public static enum FlowDirection {
		
		/**
		 * Horizontal component flow.
		 */
		RIGHT,
		
		/**
		 * Vertical component flow.
		 */
		DOWN;
		
		FlowDirection() {
			;
		}
		
	}
	
	/**
	 * An enum that holds parameters for components of the FlowBox.
	 */
	public enum FlowParam {
		
		/**
		 * Makes the component only use up its preferred size or less.
		 */
		COMPACT,
		
		/**
		 * Makes the component take up as much space as possible.
		 */
		FILL,
		
		/**
		 * Similar to the compact parameter but the component won't shrink.
		 * Fixed size components can cause the container to overflow.
		 */
		COMPACT_FIXED;
		
		FlowParam() {
			;
		}
		
	}
	
}
