/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;

import de.sogomn.spjgl.common.AbstractListenerContainer;
import de.sogomn.spjgl.common.ResizeBehavior;
import de.sogomn.spjgl.util.ImageUtils;

/**
 * Screen to draw graphics on.
 * Uses JFrame and Canvas classes internally.
 * Uses double buffering.
 * Methods like {@link #show()}, {@link #hide()} and {@link #close()} are not synchronized with the AWT even queue! Do that manually!
 * @author Sogomn
 */
public final class Screen extends AbstractListenerContainer<IDrawable> {
	
	private JFrame frame;
	private Canvas canvas;
	private VolatileImage screenImage;
	private Mouse mouse;
	private Keyboard keyboard;
	
	private boolean open;
	private int canvasWidth, canvasHeight;
	private int innerWidth, innerHeight;
	private int screenImageWidth, screenImageHeight;
	private int renderX, renderY;
	private ResizeBehavior resizeBehavior;
	private boolean redrawOnResize;
	
	private Runnable closingCallback;
	
	private static final int BUFFER_COUNT = 2;
	private static final String NO_TITLE = "";
	
	/**
	 * Represents a hidden cursor.
	 */
	public static final Cursor NO_CURSOR = Toolkit.getDefaultToolkit()
		.createCustomCursor(ImageUtils.EMPTY_IMAGE, new Point(0, 0), "No cursor");
	
	/**
	 * Constructs a new Screen object with the given width, height and title.
	 * The screen can be resized.
	 * By default the content gets streched to the canvas size.
	 * @param width The screen width
	 * @param height The screen height
	 * @param title The screen title
	 */
	public Screen(final int width, final int height, final String title) {
		frame = new JFrame(title);
		canvas = new Canvas();
		mouse = new Mouse();
		keyboard = new Keyboard();
		open = true;
		canvasWidth = innerWidth = screenImageWidth = width;
		canvasHeight = innerHeight = screenImageHeight = height;
		resizeBehavior = ResizeBehavior.STRETCH;
		
		final WindowAdapter closingAdapter = new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent w) {
				close();
				
				if (closingCallback != null) {
					closingCallback.run();
				}
			}
		};
		final ComponentAdapter resizeAdapter = new ComponentAdapter() {
			@Override
			public void componentResized(final ComponentEvent c) {
				calculateViewport();
				
				if (redrawOnResize) {
					redraw();
				}
			}
		};
		
		canvas.setPreferredSize(new Dimension(width, height));
		canvas.setIgnoreRepaint(true);
		canvas.addMouseListener(mouse);
		canvas.addMouseMotionListener(mouse);
		canvas.addMouseWheelListener(mouse);
		canvas.addKeyListener(keyboard);
		frame.addMouseListener(mouse);
		frame.addMouseMotionListener(mouse);
		frame.addMouseWheelListener(mouse);
		frame.addKeyListener(keyboard);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setIgnoreRepaint(true);
		frame.addWindowListener(closingAdapter);
		frame.addComponentListener(resizeAdapter);
		frame.add(canvas);
		frame.pack();
		frame.setLocationRelativeTo(null);
	}
	
	/**
	 * Constructs a new Screen object with the given width, height and no title.
	 * @param width The screen width
	 * @param height The screen height
	 */
	public Screen(final int width, final int height) {
		this(width, height, NO_TITLE);
	}
	
	private void calculateViewport() {
		canvasWidth = canvas.getWidth();
		canvasHeight = canvas.getHeight();
		
		if (resizeBehavior == ResizeBehavior.STRETCH) {
			stretchContent();
		} else if (resizeBehavior == ResizeBehavior.KEEP_ASPECT_RATIO) {
			fitContent();
		} else if (resizeBehavior == ResizeBehavior.KEEP_SIZE) {
			centerContent();
		}
		
		renderX = (canvasWidth / 2) - (innerWidth / 2);
		renderY = (canvasHeight / 2) - (innerHeight / 2);
		
		final double scaleX = (double)innerWidth / screenImageWidth;
		final double scaleY = (double)innerHeight / screenImageHeight;
		
		mouse.setScale(scaleX, scaleY);
		mouse.setOffset(renderX, renderY);
	}
	
	private void stretchContent() {
		innerWidth = canvasWidth;
		innerHeight = canvasHeight;
	}
	
	private void fitContent() {
		final double ratioX = (double)canvasWidth / screenImageWidth;
		final double ratioY = (double)canvasHeight / screenImageHeight;
		
		if (ratioX < ratioY) {
			innerWidth = (int)(screenImageWidth * ratioX);
			innerHeight = (int)(screenImageHeight * ratioX);
		} else if (ratioY < ratioX) {
			innerWidth = (int)(screenImageWidth * ratioY);
			innerHeight = (int)(screenImageHeight * ratioY);
		} else {
			innerWidth = (int)(screenImageWidth * ratioX);
			innerHeight = (int)(screenImageHeight * ratioY);
		}
	}
	
	private void centerContent() {
		innerWidth = screenImageWidth;
		innerHeight = screenImageHeight;
	}
	
	private VolatileImage createScreenImage() {
		final GraphicsConfiguration graphicsConfiguration = canvas.getGraphicsConfiguration();
		final VolatileImage image = graphicsConfiguration.createCompatibleVolatileImage(screenImageWidth, screenImageHeight);
		
		image.setAccelerationPriority(1);
		
		return image;
	}
	
	private int validateScreenImage(final VolatileImage image) {
		if (image == null) {
			return VolatileImage.IMAGE_INCOMPATIBLE;
		}
		
		final GraphicsConfiguration graphicsConfiguration = canvas.getGraphicsConfiguration();
		final int returnCode = image.validate(graphicsConfiguration);
		
		return returnCode;
	}
	
	private void drawOffscreen() {
		do {
			final int returnCode = validateScreenImage(screenImage);
			
			if (returnCode == VolatileImage.IMAGE_INCOMPATIBLE) {
				screenImage = createScreenImage();
				
				continue;
			}
			
			final Graphics2D screenImageGraphics = screenImage.createGraphics();
			
			ImageUtils.applyLowGraphics(screenImageGraphics);
			
			screenImageGraphics.clearRect(0, 0, screenImageWidth, screenImageHeight);
			screenImageGraphics.clipRect(0, 0, screenImageWidth, screenImageHeight);
			
			notifyListeners(drawable -> drawable.draw(screenImageGraphics));
			
			screenImageGraphics.dispose();
		} while (screenImage.contentsLost());
	}
	
	private void drawToScreen() {
		final BufferStrategy bufferStrategy = canvas.getBufferStrategy();
		
		do {
			final int returnCode = validateScreenImage(screenImage);
			
			if (returnCode != VolatileImage.IMAGE_OK) {
				redraw();
				
				return;
			}
			
			final Graphics2D canvasGraphics = (Graphics2D)bufferStrategy.getDrawGraphics();
			
			ImageUtils.applyLowGraphics(canvasGraphics);
			
			canvasGraphics.clearRect(0, 0, canvasWidth, canvasHeight);
			canvasGraphics.drawImage(screenImage, renderX, renderY, innerWidth, innerHeight, null);
			canvasGraphics.dispose();
		} while (bufferStrategy.contentsLost());
		
		bufferStrategy.show();
	}
	
	/**
	 * Validates the screen image and creates a new one, if incompatible.
	 * Then clears the screen image.
	 * Then notifies all listening IDrawable objects.
	 * Then swaps buffers.
	 * If invoked while the screen is performing some other task this will most likely throw an exception.
	 * Care about synchronization!
	 */
	public void redraw() {
		if (!isOpen() || !isVisible()) {
			return;
		}
		
		drawOffscreen();
		drawToScreen();
	}
	
	/**
	 * Shows the screen and initializes the graphics.
	 * Does nothing if the screen is already visible.
	 */
	public void show() {
		if (!isOpen() || isVisible()) {
			return;
		}
		
		frame.setVisible(true);
		screenImage = createScreenImage();
		canvas.createBufferStrategy(BUFFER_COUNT);
		canvas.requestFocusInWindow();
		
		calculateViewport();
		redraw();
	}
	
	/**
	 * Hides the screen. It can not be updated anymore.
	 * Does nothing if the screen is not visible.
	 */
	public void hide() {
		if (!isOpen() || !isVisible()) {
			return;
		}
		
		frame.setVisible(false);
	}
	
	/**
	 * Closes the screen.
	 * It can't be shown again.
	 */
	public void close() {
		if (!isOpen()) {
			return;
		}
		
		setFullScreen(false);
		
		frame.setVisible(false);
		frame.dispose();
		open = false;
	}
	
	/**
	 * Centers the screen.
	 */
	public void center() {
		frame.setLocationRelativeTo(null);
	}
	
	/**
	 * Sets the callback for the closing event of the screen.
	 * Only gets called when the user closes it natively (e.g. clicks the X button).
	 * Does not get called by {@link #close()}.
	 * @param closingCallback The callback
	 */
	public void setClosingCallback(final Runnable closingCallback) {
		this.closingCallback = closingCallback;
	}
	
	/**
	 * If true and supported, this will toggle full screen mode.
	 * This will cap the drawing rate to what the default display configuration supports.
	 * It is highly recommended to set the screen to non-resizable.
	 * There may be performance issues.
	 * Does nothing if the screen is not visible.
	 * @param fullScreen The full screen flag
	 */
	public void setFullScreen(final boolean fullScreen) {
		if (!isVisible() || !isOpen()) {
			return;
		}
		
		final GraphicsDevice display = GraphicsEnvironment.getLocalGraphicsEnvironment()
			.getDefaultScreenDevice();
		final boolean fullScreenAllowed = display.isFullScreenSupported();
		
		if (fullScreen && fullScreenAllowed) {
			display.setFullScreenWindow(frame);
		} else {
			display.setFullScreenWindow(null);
		}
	}
	
	/**
	 * Sets the resizable flag of the screen.
	 * Resizes the outer frame on some platforms.
	 * There may be a small border when using the resize behavior "KEEP_ASPECT_RATIO", even though the aspect ratio is the same as the initial.
	 * @param resizable The state
	 */
	public void setResizable(final boolean resizable) {
		frame.setResizable(resizable);
		
		calculateViewport();
	}
	
	/**
	 * Sets the title of the screen.
	 * @param title The title the screen should have from now on
	 */
	public void setTitle(final String title) {
		frame.setTitle(title);
	}
	
	/**
	 * Sets the resize behavior of the screen.
	 * @param resizeBehavior The resize behavior
	 */
	public void setResizeBehavior(final ResizeBehavior resizeBehavior) {
		this.resizeBehavior = resizeBehavior;
	}
	
	/**
	 * Sets the icons of the screen.
	 * It will be displayed on the top left of the frame as well as in the taskbar.
	 * Different icons will be displayed depending on the platform capabilities.
	 * @param images The icons
	 */
	public void setIcons(final BufferedImage... images) {
		final List<BufferedImage> imageList = Arrays.asList(images);
		
		frame.setIconImages(imageList);
	}
	
	/**
	 * Sets the size of the canvas, not taking frame borders into account.
	 * @param width The target width
	 * @param height The target height
	 */
	public void setCanvasSize(final int width, final int height) {
		final Dimension size = new Dimension(width, height);
		
		canvas.setPreferredSize(size);
		frame.pack();
		canvas.requestFocusInWindow();
		
		calculateViewport();
	}
	
	/**
	 * Sets the render width of the screen.
	 * @param width The new width
	 * @param height The new height
	 */
	public void setRenderSize(final int width, final int height) {
		screenImageWidth = width;
		screenImageHeight = height;
		screenImage = createScreenImage();
		
		calculateViewport();
	}
	
	/**
	 * Sets the location of the outer frame.
	 * @param x The x coordinate
	 * @param y The y coordinate
	 */
	public void setLocation(final int x, final int y) {
		frame.setLocation(x, y);
	}
	
	/**
	 * Sets the cursor for the screen. Pass the static variable NO_CURSOR to hide it.
	 * @param cursor The new cursor
	 */
	public void setCursor(final Cursor cursor) {
		frame.setCursor(cursor);
	}
	
	/**
	 * Sets the background color of the screen.
	 * @param color The background color
	 */
	public void setBackgroundColor(final Color color) {
		canvas.setBackground(color);
	}
	
	/**
	 * Sets the flag which indicates whether the screen should redraw itself when resized.
	 * @param redrawOnResize The state
	 */
	public void setRedrawOnResize(final boolean redrawOnResize) {
		this.redrawOnResize = redrawOnResize;
	}
	
	/**
	 * Returns whether the screen is open and can be used or not.
	 * @return The state
	 */
	public boolean isOpen() {
		return open;
	}
	
	/**
	 * Returns whether the screen is visible or not.
	 * This does not respect minimization.
	 * @return The state
	 */
	public boolean isVisible() {
		return frame.isVisible();
	}
	
	/**
	 * Returns whether the screen is resizable or not.
	 * @return True if it is resizable; false otherwise
	 */
	public boolean isResizable() {
		return frame.isResizable();
	}
	
	/**
	 * Returns the full screen flag of the screen.
	 * @return True if the screen is in full screen mode; false otherwise
	 */
	public boolean isFullScreen() {
		final GraphicsDevice display = GraphicsEnvironment.getLocalGraphicsEnvironment()
			.getDefaultScreenDevice();
		final Window window = display.getFullScreenWindow();
		final boolean fullScreen = window == frame;
		
		return fullScreen;
	}
	
	/**
	 * Returns the mouse handle for this screen.
	 * It allows you to add listeners and fetch information about the cursor.
	 * @return The mouse handle for this screen
	 */
	public Mouse getMouse() {
		return mouse;
	}
	
	/**
	 * Returns the keyboard handle for this screen.
	 * It allows you to add listeners and fetch information about key presses.
	 * @return The keyboard handle for this screen
	 */
	public Keyboard getKeyboard() {
		return keyboard;
	}
	
	/**
	 * Returns whether the screen is focused or not.
	 * @return The state
	 */
	public boolean isFocused() {
		return frame.isFocused();
	}
	
	/**
	 * Returns the absolute width of the whole canvas.
	 * This does not include frame borders.
	 * @return The width
	 */
	public int getCanvasWidth() {
		return canvasWidth;
	}
	
	/**
	 * Returns the absolute height of the whole canvas.
	 * This does not include frame borders.
	 * @return The height
	 */
	public int getCanvasHeight() {
		return canvasHeight;
	}
	
	/**
	 * Returns the absolute width of the content.
	 * May differ from the canvas width depending on the resize behavior.
	 * @return The width
	 */
	public int getInnerCanvasWidth() {
		return innerWidth;
	}
	
	/**
	 * Returns the absolute height of the content.
	 * May differ from the canvas width depending on the resize behavior.
	 * @return The height
	 */
	public int getInnerCanvasHeight() {
		return innerHeight;
	}
	
	/**
	 * Returns the render width of the screen.
	 * @return The width
	 */
	public int getRenderWidth() {
		return screenImageWidth;
	}
	
	/**
	 * Returns the render height of the screen.
	 * @return The height
	 */
	public int getRenderHeight() {
		return screenImageHeight;
	}
	
	/**
	 * Returns the current title of the screen.
	 * @return The title
	 */
	public String getTitle() {
		return frame.getTitle();
	}
	
	/**
	 * Returns the current resize behavior of the screen.
	 * @return The resize behavior
	 */
	public ResizeBehavior getResizeBehavior() {
		return resizeBehavior;
	}
	
	/**
	 * Returns whether the screen should redraw itself if the window is resized.
	 * @return The state
	 */
	public boolean shouldRedrawOnResize() {
		return redrawOnResize;
	}
	
	/**
	 * An image of what's currently on the screen.
	 * @return The current screen image
	 */
	public BufferedImage getScreenImage() {
		return screenImage.getSnapshot();
	}
	
}
