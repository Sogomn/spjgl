/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.SwingUtilities;

import de.sogomn.spjgl.common.AbstractListenerContainer;

/**
 * Mouse listener to store information about current mouse inputs.
 * Turns the bloated AWT mouse events into simpler ones.
 */
public final class Mouse extends AbstractListenerContainer<IMouseListener> implements MouseListener, MouseMotionListener, MouseWheelListener {
	
	private double scaleX, scaleY;
	private int offsetX, offsetY;
	
	private int mouseX, mouseY;
	private boolean leftPressed, middlePressed, rightPressed;
	
	private static final boolean PRESSED = true;
	private static final boolean RELEASED = false;
	private static final int NO_SCALE = 1;
	private static final int NO_OFFSET = 0;
	
	Mouse() {
		scaleX = scaleY = NO_SCALE;
		offsetX = offsetY = NO_OFFSET;
	}
	
	private int getRelativeX(final int x) {
		final int relativeX = (int)(x / scaleX - offsetX / scaleX);
		
		return relativeX;
	}
	
	private int getRelativeY(final int y) {
		final int relativeY = (int)(y / scaleY - offsetY / scaleY);
		
		return relativeY;
	}
	
	private void fireMouseEvent(final MouseEvent m, final boolean flag) {
		final int x = getRelativeX(m.getX());
		final int y = getRelativeY(m.getY());
		final int button = m.getButton();
		
		if (SwingUtilities.isLeftMouseButton(m)) {
			leftPressed = flag;
		} else if (SwingUtilities.isMiddleMouseButton(m)) {
			middlePressed = flag;
		} else if (SwingUtilities.isRightMouseButton(m)) {
			rightPressed = flag;
		}
		
		notifyListeners(listener -> listener.mouseEvent(x, y, button, flag));
	}
	
	private void fireMouseMovedEvent(final MouseEvent m) {
		final int x = getRelativeX(m.getX());
		final int y = getRelativeY(m.getY());
		final int modifiers = m.getModifiersEx();
		
		mouseX = x;
		mouseY = y;
		
		notifyListeners(listener -> listener.mouseMotionEvent(x, y, modifiers));
	}
	
	private void fireMouseWheelEvent(final MouseWheelEvent m) {
		final int x = getRelativeX(m.getX());
		final int y = getRelativeY(m.getY());
		final int rotation = m.getWheelRotation();
		
		notifyListeners(listener -> listener.mouseWheelEvent(x, y, rotation));
	}
	
	void setScale(final double scaleX, final double scaleY) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;
	}
	
	void setOffset(final int offsetX, final int offsetY) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}
	
	@Override
	public void mousePressed(final MouseEvent m) {
		fireMouseEvent(m, PRESSED);
	}
	
	@Override
	public void mouseReleased(final MouseEvent m) {
		fireMouseEvent(m, RELEASED);
	}
	
	@Override
	public void mouseMoved(final MouseEvent m) {
		fireMouseMovedEvent(m);
	}
	
	@Override
	public void mouseDragged(final MouseEvent m) {
		fireMouseMovedEvent(m);
	}
	
	@Override
	public void mouseWheelMoved(final MouseWheelEvent m) {
		fireMouseWheelEvent(m);
	}
	
	@Override
	public void mouseClicked(final MouseEvent m) {
		//...
	}
	
	@Override
	public void mouseEntered(final MouseEvent m) {
		//...
	}
	
	@Override
	public void mouseExited(final MouseEvent m) {
		//...
	}
	
	/**
	 * Return the current x coordinate of the mouse position.
	 * @return The current x coordinate of the mouse position
	 */
	public int getMouseX() {
		return mouseX;
	}
	
	/**
	 * Return the current y coordinate of the mouse position.
	 * @return The current y coordinate of the mouse position
	 */
	public int getMouseY() {
		return mouseY;
	}
	
	/**
	 * Returns whether or not the left button is currently being pressed.
	 * @return True if left is being pressed, false otherwise
	 */
	public boolean isLeftPressed() {
		return leftPressed;
	}
	
	/**
	 * Returns whether or not the middle button is currently being pressed.
	 * @return True if middle is being pressed, false otherwise
	 */
	public boolean isMiddlePressed() {
		return middlePressed;
	}
	
	/**
	 * Returns whether or not the right button is currently being pressed.
	 * @return True if right is being pressed, false otherwise
	 */
	public boolean isRightPressed() {
		return rightPressed;
	}
	
}
