/*******************************************************************************
 * Copyright 2018 Johannes Boczek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package de.sogomn.spjgl;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import de.sogomn.spjgl.common.AbstractListenerContainer;

/**
 * Keyboard listener to store information about current keyboard inputs.
 * Turns the bloated AWT keyboard events into simpler ones.
 */
public final class Keyboard extends AbstractListenerContainer<IKeyboardListener> implements KeyListener {
	
	private boolean[] keyStates;
	
	private static final boolean PRESSED = true;
	private static final boolean RELEASED = false;
	
	Keyboard() {
		keyStates = new boolean[Character.MAX_VALUE];
	}
	
	private void fireKeyboardEvent(final KeyEvent k, final boolean flag) {
		final int key = k.getKeyCode();
		final char c = k.getKeyChar();
		final int modifiers = k.getModifiersEx();
		
		keyStates[key] = flag;
		
		notifyListeners(listener -> listener.keyboardEvent(key, c, modifiers, flag));
	}
	
	@Override
	public void keyPressed(final KeyEvent k) {
		fireKeyboardEvent(k, PRESSED);
	}
	
	@Override
	public void keyReleased(final KeyEvent k) {
		fireKeyboardEvent(k, RELEASED);
	}
	
	@Override
	public void keyTyped(final KeyEvent k) {
		//...
	}
	
	/**
	 * Returns whether or not the specified key is currently being pressed.
	 * @param keyCode The key code to check
	 * @return Whether or not the specified key is currently being pressed
	 */
	public boolean isKeyPressed(final int keyCode) {
		return keyStates[keyCode];
	}
	
}
