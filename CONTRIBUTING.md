# Contribution guidelines
Because we don't like each other's coding style.

---

## 1.
```
void foo(final Foo arg) {
	final Fuzz var = new Fuzz();
	
	arg.bar(var);
}
```

## 2.
Hard tabs.

## 3.
Use final. Everywhere.

## 4.
1. Fields
2. Constants
3. Static blocks
4. Constructors
5. Methods
6. Setters
7. Getters
8. Static methods
9. Classes
10. Enums
11. Static classes

Private before default before protected before public.

## 5.
No single-line if statements or loops.

## 6.
KISS principle.

## 7.
Extra blank likes don't hurt.

## 8.
If code gets messy, explain yourself.

## 9.
Avoid magic values other than obvious ones.
Use class constants.
